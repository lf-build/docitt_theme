FROM microsoft/dotnet:2.1.302-sdk AS build-env
ARG RESTORE
RUN : "${RESTORE:?Build argument needs to be set and non-empty.}"
WORKDIR /app

COPY ./src/ /app/
COPY ./Directory.Build.props /app


WORKDIR /app/Docitt.Theme.Api
RUN $RESTORE && dotnet publish -c Release -o out --no-restore

# Build runtime image
FROM microsoft/dotnet:2.1.2-aspnetcore-runtime
WORKDIR /app/
COPY --from=build-env /app/Docitt.Theme.Api/out .

# Setup NodeJs
RUN apt-get update && \
    apt-get install -y wget && \
    apt-get install -y gnupg2 && \
    wget -qO- https://deb.nodesource.com/setup_8.x | bash - && \
    apt-get install -y build-essential nodejs
# End setup
RUN apt-get install npm
RUN npm install less -g
RUN npm install uglifycss -g

ENTRYPOINT ["dotnet", "Docitt.Theme.Api.dll"]