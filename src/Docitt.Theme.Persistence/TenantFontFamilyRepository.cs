using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Persistence.Mongo;
using LendFoundry.Security.Tokens;
using LendFoundry.Tenant.Client;
using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using MongoDB.Driver;
using MongoDB.Driver.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Docitt.Theme
{
    public class TenantFontFamilyRepository : MongoRepository<ITenantFontFamily, TenantFontFamily>, ITenantFontFamilyRepository
    {
        static TenantFontFamilyRepository()
        {
            BsonClassMap.RegisterClassMap<TenantFontFamily>(map =>
            {
                map.AutoMap();
                var type = typeof(TenantFontFamily);
                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
                map.SetIsRootClass(true);
            });
        }
        public TenantFontFamilyRepository(ITenantService tenantService
                                    , IMongoConfiguration configuration
                                    , ITenantTime tenantTime
                                    , ITokenReader tokenReader
                                    , ITokenHandler tokenHandler) : base(tenantService, configuration, "tenantfontfamily")
        {
            if (configuration == null)
                throw new ArgumentNullException(nameof(configuration));

            if (string.IsNullOrWhiteSpace(configuration.ConnectionString))
                throw new ArgumentException("Connection String cannot be empty");

            if (string.IsNullOrWhiteSpace(configuration.Database))
                throw new ArgumentException("Database name cannot be empty");

            TenantService = tenantService ?? throw new ArgumentNullException(nameof(tenantService));
            TenantTime = tenantTime ?? throw new ArgumentNullException(nameof(tenantTime));
            TokenReader = tokenReader ?? throw new ArgumentNullException(nameof(tokenReader));
            TokenHandler = tokenHandler ?? throw new ArgumentNullException(nameof(tokenHandler));
        }

        private ITenantService TenantService { get; }
        private ITenantTime TenantTime { get; }
        private ITokenReader TokenReader { get; }
        private ITokenHandler TokenHandler { get; }

        public async Task<ITenantFontFamily> GetTenantFontFamily()
        {
            return await Query.FirstOrDefaultAsync();
        }

        public async Task<ITenantFontFamily> ApplyTenantFontFamily(ITenantFontFamily tenantFontFamily)
        {
            if (tenantFontFamily == null)
                throw new ArgumentNullException(nameof(tenantFontFamily));
            if (string.IsNullOrWhiteSpace(tenantFontFamily.Name))
                throw new ArgumentNullException(nameof(tenantFontFamily.Name));
            if (string.IsNullOrWhiteSpace(tenantFontFamily.SourceFontUrl))
                throw new ArgumentNullException(nameof(tenantFontFamily.SourceFontUrl));
            if (tenantFontFamily.FontWeight == null || tenantFontFamily.FontWeight.Count == 0)
                throw new ArgumentNullException(nameof(tenantFontFamily.FontWeight));

            EnsureFontWeights(tenantFontFamily.FontWeight);

            try
            {
                var result = Query.FirstOrDefault();
                if (result == null)
                {
                    tenantFontFamily.Id = ObjectId.GenerateNewId().ToString();

                    var record = new TenantFontFamily
                    {
                        Id = tenantFontFamily.Id,
                        Name = tenantFontFamily.Name,
                        SourceFontUrl = tenantFontFamily.SourceFontUrl,
                        FontWeight = tenantFontFamily.FontWeight,
                        AppliedBy = ExtractCurrentUser(),
                        AppliedOn = TenantTime.Now,
                        TenantId = TenantService.Current.Id
                    };
                    await Collection.InsertOneAsync(record);
                }
                else
                {
                    await Collection.UpdateOneAsync(Builders<ITenantFontFamily>.Filter.Where(a => a.TenantId == TenantService.Current.Id),
                                                    Builders<ITenantFontFamily>.Update
                                                            .Set(a => a.Name, tenantFontFamily.Name)
                                                            .Set(a => a.SourceFontUrl, tenantFontFamily.SourceFontUrl)
                                                            .Set(a => a.FontWeight, tenantFontFamily.FontWeight)
                                                            .Set(a => a.AppliedOn, TenantTime.Now)
                                                            .Set(a => a.AppliedBy, ExtractCurrentUser()));

                }
                return await Task.Run(() => Query.FirstOrDefault());
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        private string ExtractCurrentUser()
        {
            var token = TokenHandler.Parse(TokenReader.Read());
            var username = token?.Subject;
            //if (string.IsNullOrWhiteSpace(token?.Subject))
            //    throw new ArgumentException("User is not authorized");
            return username;
        }

        private void EnsureFontWeights(List<string> fontWeights)
        {
            foreach (var fontWeight in fontWeights)
            {
                if (string.IsNullOrWhiteSpace(fontWeight))
                {
                    throw new ArgumentNullException(nameof(fontWeight));
                }
            }
        }
    }
}