﻿using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Persistence.Mongo;
using LendFoundry.Security.Tokens;
using LendFoundry.Tenant.Client;
using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using MongoDB.Bson.Serialization.Serializers;
using MongoDB.Driver;
using MongoDB.Driver.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Docitt.Theme
{
    public class AssetRepository : MongoRepository<IAsset, Asset>, IAssetRepository
    {
        static AssetRepository()
        {
            BsonClassMap.RegisterClassMap<Asset>(map =>
            {
                map.AutoMap();
                map.SetIgnoreExtraElements(true);
                map.UnmapMember(a => a.WelcomeScreenBackgroundImageUrl);
                map.UnmapMember(a => a.WelcomScreenImageUrl);
                map.MapProperty(a => a.AppliedOn).SetSerializer(new NullableSerializer<DateTimeOffset>());
                var type = typeof(IAsset);
                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
                map.SetIsRootClass(true);
            });
        }

        public AssetRepository(ITenantService tenantService
                                    , IMongoConfiguration configuration
                                    , ITenantTime tenantTime
                                    , ITokenReader tokenReader
                                    , ITokenHandler tokenHandler) : base(tenantService, configuration, "assets")
        {
            if (configuration == null)
                throw new ArgumentNullException(nameof(configuration));

            if (string.IsNullOrWhiteSpace(configuration.ConnectionString))
                throw new ArgumentException("Connection String cannot be empty");

            if (string.IsNullOrWhiteSpace(configuration.Database))
                throw new ArgumentException("Database name cannot be empty");

            TenantService = tenantService ?? throw new ArgumentNullException(nameof(tenantService));
            TenantTime = tenantTime ?? throw new ArgumentNullException(nameof(tenantTime));
            TokenReader = tokenReader ?? throw new ArgumentNullException(nameof(tokenReader));
            TokenHandler = tokenHandler ?? throw new ArgumentNullException(nameof(tokenHandler));
        }

        private ITenantService TenantService { get; }
        private ITenantTime TenantTime { get; }
        private ITokenReader TokenReader { get; }
        private ITokenHandler TokenHandler { get; }

        public async Task<IAsset> GetDefaultAsset()
        {
            return await Query.Where(x => x.IsDefault == true).FirstOrDefaultAsync();
        }
        public async Task<IAsset> GetCurrentAsset()
        {
            return await Query.Where(x => x.IsApplied == true).FirstOrDefaultAsync();
        }

        public async Task<IAsset> AddAsset(IAsset asset)
        {
            if (asset == null)
                throw new ArgumentNullException(nameof(asset));
            if (asset.IsWelcomScreenVideoOn && string.IsNullOrWhiteSpace(asset.WelcomScreenVideoUrl))
                throw new ArgumentNullException(nameof(asset.WelcomScreenVideoUrl));
            if (asset.IsWelcomScreenVideoOn == false && string.IsNullOrWhiteSpace(asset.WelcomScreenImageName))
                throw new ArgumentNullException(nameof(asset.WelcomScreenImageName));
            if (asset.IsWelcomeScreenBackgroundImageOn && string.IsNullOrWhiteSpace(asset.WelcomeScreenBackgroundImageName))
                throw new ArgumentNullException(nameof(asset.WelcomeScreenBackgroundImageName));
            try
            {
                if (asset.IsApplied)
                {
                    var result = await Collection.UpdateOneAsync(Builders<IAsset>.Filter.Where(a => a.TenantId == TenantService.Current.Id
                                                            && a.IsApplied == true),
                                                     Builders<IAsset>.Update
                                                             .Set(a => a.IsApplied, false)
                                                             .Set(a => a.AppliedOn, null)
                                                             .Set(a => a.AppliedBy, string.Empty));
                }

                if (asset.IsDefault)
                {
                    var result = Query.FirstOrDefault(x => x.IsDefault == true);
                    if (result == null)
                    {
                        asset.Id = ObjectId.GenerateNewId().ToString();

                        var entry = new Asset
                        {
                            Id = asset.Id,
                            IsDefault = asset.IsDefault,
                            IsApplied = asset.IsApplied,
                            WelcomeScreenBackgroundImageName = asset.WelcomeScreenBackgroundImageName,
                            IsWelcomeScreenBackgroundImageOn = asset.IsWelcomeScreenBackgroundImageOn,
                            WelcomScreenVideoUrl = asset.WelcomScreenVideoUrl,
                            WelcomScreenImageName = asset.WelcomScreenImageName,
                            IsWelcomScreenVideoOn = asset.IsWelcomScreenVideoOn,
                            AppliedBy = ExtractCurrentUser(),
                            AppliedOn = TenantTime.Now,
                            TenantId = TenantService.Current.Id,
                            AlternateUserLogo = asset.AlternateUserLogoUrl
                        };
                        await Collection.InsertOneAsync(entry);
                    }
                    else
                    {
                        var result1 = await Collection.UpdateOneAsync(Builders<IAsset>.Filter.Where(a => a.TenantId == TenantService.Current.Id
                                                                    && a.IsDefault == true),
                                                            Builders<IAsset>.Update
                                                                    .Set(a => a.WelcomeScreenBackgroundImageName, asset.WelcomeScreenBackgroundImageName)
                                                                    .Set(a => a.IsWelcomeScreenBackgroundImageOn, asset.IsWelcomeScreenBackgroundImageOn)
                                                                    .Set(a => a.WelcomScreenVideoUrl, asset.WelcomScreenVideoUrl)
                                                                    .Set(a => a.WelcomScreenImageName, asset.WelcomScreenImageName)
                                                                    .Set(a => a.IsWelcomScreenVideoOn, asset.IsWelcomScreenVideoOn)
                                                                    .Set(a => a.AlternateUserLogoUrl, asset.AlternateUserLogoUrl)
                                                                    .Set(a => a.IsApplied, asset.IsApplied)
                                                                    .Set(a => a.AppliedBy, ExtractCurrentUser())
                                                                    .Set(a => a.AppliedOn, TenantTime.Now));
                    }

                    return await Task.Run(() => Query.Where(x => x.IsDefault == true).FirstOrDefault());
                }
                else
                {
                    var result = Query.FirstOrDefault(x => x.IsDefault == false);
                    if (result == null)
                    {
                        var record = new Asset
                        {
                            Id = asset.Id,
                            IsDefault = asset.IsDefault,
                            IsApplied = asset.IsApplied,
                            WelcomeScreenBackgroundImageName = asset.WelcomeScreenBackgroundImageName,
                            IsWelcomeScreenBackgroundImageOn = asset.IsWelcomeScreenBackgroundImageOn,
                            WelcomScreenVideoUrl = asset.WelcomScreenVideoUrl,
                            WelcomScreenImageName = asset.WelcomScreenImageName,
                            IsWelcomScreenVideoOn = asset.IsWelcomScreenVideoOn,
                            AppliedBy = ExtractCurrentUser(),
                            AppliedOn = TenantTime.Now,
                            TenantId = TenantService.Current.Id,
                            AlternateUserLogo = asset.AlternateUserLogoUrl
                        };
                        await Collection.InsertOneAsync(record);
                    }
                    else
                    {
                        var result1 = await Collection.UpdateOneAsync(Builders<IAsset>.Filter.Where(a => a.TenantId == TenantService.Current.Id
                                                                    && a.IsDefault == false),
                                                            Builders<IAsset>.Update
                                                                    .Set(a => a.WelcomeScreenBackgroundImageName, asset.WelcomeScreenBackgroundImageName)
                                                                    .Set(a => a.IsWelcomeScreenBackgroundImageOn, asset.IsWelcomeScreenBackgroundImageOn)
                                                                    .Set(a => a.WelcomScreenVideoUrl, asset.WelcomScreenVideoUrl)
                                                                    .Set(a => a.WelcomScreenImageName, asset.WelcomScreenImageName)
                                                                    .Set(a => a.IsWelcomScreenVideoOn, asset.IsWelcomScreenVideoOn)
                                                                    .Set(a => a.AlternateUserLogoUrl, asset.AlternateUserLogoUrl)
                                                                    .Set(a => a.IsApplied, asset.IsApplied)
                                                                    .Set(a => a.AppliedBy, ExtractCurrentUser())
                                                                    .Set(a => a.AppliedOn, TenantTime.Now));
                    }
                    return await Task.Run(() => Query.Where(x => x.IsDefault == false).FirstOrDefault());
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        private string ExtractCurrentUser()
        {
            var token = TokenHandler.Parse(TokenReader.Read());
            var username = token?.Subject;
            //if (string.IsNullOrWhiteSpace(token?.Subject))
            //    throw new ArgumentException("User is not authorized");
            return username;
        }

        public async Task<List<IAsset>> GetAllAssets()
        {
            return await Query.ToListAsync();
        }
    }
}
