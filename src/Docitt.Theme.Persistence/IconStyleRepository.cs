using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Persistence.Mongo;
using LendFoundry.Security.Tokens;
using LendFoundry.Tenant.Client;
using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using MongoDB.Bson.Serialization.Serializers;
using MongoDB.Driver;
using MongoDB.Driver.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Docitt.Theme
{
    public class IconStyleRepository : MongoRepository<IIconStyle, IconStyle>, IIconStyleRepository
    {
        static IconStyleRepository()
        {
            BsonClassMap.RegisterClassMap<IconStyle>(map =>
            {
                map.AutoMap();
                map.SetIgnoreExtraElements(true);
                map.UnmapMember(a => a.IconSourceUrl);
                map.MapProperty(a => a.AppliedOn).SetSerializer(new NullableSerializer<DateTimeOffset>());
                var type = typeof(IconStyle);
                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
                map.SetIsRootClass(true);
            });
        }
        public IconStyleRepository(ITenantService tenantService
                                    , IMongoConfiguration configuration
                                    , ITenantTime tenantTime
                                    , ITokenReader tokenReader
                                    , ITokenHandler tokenHandler) : base(tenantService, configuration, "iconstyles")
        {
            if (configuration == null)
                throw new ArgumentNullException(nameof(configuration));

            if (string.IsNullOrWhiteSpace(configuration.ConnectionString))
                throw new ArgumentException("Connection String cannot be empty");

            if (string.IsNullOrWhiteSpace(configuration.Database))
                throw new ArgumentException("Database name cannot be empty");

            TenantService = tenantService ?? throw new ArgumentNullException(nameof(tenantService));
            TenantTime = tenantTime ?? throw new ArgumentNullException(nameof(tenantTime));
            TokenReader = tokenReader ?? throw new ArgumentNullException(nameof(tokenReader));
            TokenHandler = tokenHandler ?? throw new ArgumentNullException(nameof(tokenHandler));
        }

        private ITenantService TenantService { get; }
        private ITenantTime TenantTime { get; }
        private ITokenReader TokenReader { get; }
        private ITokenHandler TokenHandler { get; }

        public async Task<IIconStyle> GetDefaultIconStyle()
        {
            return await Query.Where(x => x.IsDefault == true).FirstOrDefaultAsync();
        }
        public async Task<IIconStyle> GetCurrentIconStyle()
        {
            return await Query.Where(x => x.IsApplied == true).FirstOrDefaultAsync();
        }
        public async Task<List<IIconStyle>> GetIconStyleByName(string iconStyleName)
        {
            if (string.IsNullOrWhiteSpace(iconStyleName))
                throw new ArgumentNullException(nameof(iconStyleName));

            return await Query.Where(x => x.Name == iconStyleName.ToLower()).ToListAsync();
        }
        public async Task<List<IIconStyle>> GetAllIconStyle()
        {
            return await Query.ToListAsync();
        }
        public async Task<IIconStyle> AddIconStyle(IIconStyle iconStyle)
        {
            if (iconStyle == null)
                throw new ArgumentNullException(nameof(iconStyle));
            if (string.IsNullOrWhiteSpace(iconStyle.Name))
                throw new ArgumentNullException(nameof(iconStyle.Name));
            try
            {
                if (iconStyle.IsDefault)
                {
                    // Reset default iconstyle.
                    await Collection.UpdateOneAsync(Builders<IIconStyle>.Filter.Where(a => a.TenantId == TenantService.Current.Id
                                                            && a.IsDefault == true),
                                                    Builders<IIconStyle>.Update
                                                            .Set(a => a.IsDefault, false)
                                                            .Set(a => a.UpdatedOn, TenantTime.Now)
                                                            .Set(a => a.UpdatedBy, ExtractCurrentUser()));
                }
                if (iconStyle.IsApplied)
                {
                    // Reset applied iconstyle.
                    await Collection.UpdateManyAsync(Builders<IIconStyle>.Filter.Where(a => a.TenantId == TenantService.Current.Id
                                                            && a.IsApplied == true),
                                                    Builders<IIconStyle>.Update
                                                            .Set(a => a.IsApplied, false)
                                                            .Set(a => a.AppliedOn, TenantTime.Now)
                                                            .Set(a => a.AppliedBy, ExtractCurrentUser()));
                }

                var result = Query.FirstOrDefault(x => x.Name.ToLower() == iconStyle.Name.ToLower());
                if (result == null)
                {
                    iconStyle.Id = ObjectId.GenerateNewId().ToString();
                    var record = new IconStyle
                    {
                        Id = iconStyle.Id,
                        Name = iconStyle.Name,
                        IsDefault = iconStyle.IsDefault,
                        IsApplied = iconStyle.IsApplied,
                        CreatedBy = ExtractCurrentUser(),
                        CreatedOn = TenantTime.Now,
                        TenantId = TenantService.Current.Id
                    };
                    await Collection.InsertOneAsync(record);
                }
                else
                {
                    await Collection.UpdateOneAsync(Builders<IIconStyle>.Filter.Where(a => a.TenantId == TenantService.Current.Id
                                                            && a.Name.ToLower() == iconStyle.Name.ToLower()),
                                                    Builders<IIconStyle>.Update
                                                            .Set(a => a.IsDefault, iconStyle.IsDefault)
                                                            .Set(a => a.IsApplied, iconStyle.IsApplied)
                                                            .Set(a => a.UpdatedOn, TenantTime.Now)
                                                            .Set(a => a.UpdatedBy, ExtractCurrentUser()));

                }
                return await Task.Run(() => Query.Where(x => x.Name.ToLower() == iconStyle.Name.ToLower()).FirstOrDefault());
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        public async Task<bool> ApplyIconStyle(string iconStyleName)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(iconStyleName))
                    throw new ArgumentNullException(nameof(iconStyleName));

                var result = await Collection.UpdateOneAsync(Builders<IIconStyle>.Filter.Where(a => a.TenantId == TenantService.Current.Id
                                                            && a.Name.ToLower() == iconStyleName.ToLower()),
                                                    Builders<IIconStyle>.Update
                                                            .Set(a => a.IsApplied, true)
                                                            .Set(a => a.AppliedOn, TenantTime.Now)
                                                            .Set(a => a.AppliedBy, ExtractCurrentUser()));
                if (result.ModifiedCount == 1)
                {
                    var result1 = await Collection.UpdateOneAsync(Builders<IIconStyle>.Filter.Where(a => a.TenantId == TenantService.Current.Id
                                                            && a.Name.ToLower() != iconStyleName.ToLower()
                                                            && a.IsApplied == true),
                                                    Builders<IIconStyle>.Update
                                                            .Set(a => a.IsApplied, false)
                                                            .Set(a => a.AppliedBy, string.Empty)
                                                            .Set(a => a.AppliedOn, null));
                }
                else
                {
                    throw new ArgumentException("No icon style was found with the name " + iconStyleName);
                }
                return true;
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        private string ExtractCurrentUser()
        {
            var token = TokenHandler.Parse(TokenReader.Read());
            var username = token?.Subject;
            //if (string.IsNullOrWhiteSpace(token?.Subject))
            //    throw new ArgumentException("User is not authorized");
            return username;
        }
    }
}