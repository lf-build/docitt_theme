using AutoMapper;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Persistence.Mongo;
using LendFoundry.Security.Tokens;
using LendFoundry.Tenant.Client;
using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using MongoDB.Bson.Serialization.Serializers;
using MongoDB.Driver;
using MongoDB.Driver.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Docitt.Theme
{
    public class MasterFontFamilyRepository : MongoRepository<IMasterFontFamily, MasterFontFamily>, IMasterFontFamilyRepository
    {
        static MasterFontFamilyRepository()
        {
            BsonClassMap.RegisterClassMap<MasterFontFamily>(map =>
            {
                map.AutoMap();
                var type = typeof(MasterFontFamily);
                map.MapProperty(a => a.UpdatedOn).SetSerializer(new NullableSerializer<DateTimeOffset>());
                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
                map.SetIsRootClass(true);
            });
        }
        public MasterFontFamilyRepository(ITenantService tenantService
                                    , IMongoConfiguration configuration
                                    , ITenantTime tenantTime
                                    , ITokenReader tokenReader
                                    , ITokenHandler tokenHandler) : base(tenantService, configuration, "masterfontfamily")
        {
            if (configuration == null)
                throw new ArgumentNullException(nameof(configuration));

            if (string.IsNullOrWhiteSpace(configuration.ConnectionString))
                throw new ArgumentException("Connection String cannot be empty");

            if (string.IsNullOrWhiteSpace(configuration.Database))
                throw new ArgumentException("Database name cannot be empty");

            TenantService = tenantService ?? throw new ArgumentNullException(nameof(tenantService));
            TenantTime = tenantTime ?? throw new ArgumentNullException(nameof(tenantTime));
            TokenReader = tokenReader ?? throw new ArgumentNullException(nameof(tokenReader));
            TokenHandler = tokenHandler ?? throw new ArgumentNullException(nameof(tokenHandler));

            var client = new MongoClient(configuration.ConnectionString);
            var database = client.GetDatabase(configuration.Database);
            Collection = database.GetCollection<MasterFontFamily>("masterfontfamily");

            var config = new MapperConfiguration(cfg => { cfg.CreateMap<IMasterFontFamily, MasterFontFamily>(); });
            config.CreateMapper().Map<MasterFontFamily, IMasterFontFamily>(new MasterFontFamily());
        }

        private ITenantService TenantService { get; }
        private ITenantTime TenantTime { get; }
        private ITokenReader TokenReader { get; }
        private ITokenHandler TokenHandler { get; }
        private IMongoCollection<MasterFontFamily> Collection { get; }
        public async Task<IMasterFontFamily> GetDefaultMasterFontFamily()
        {
            var builder = new FilterDefinitionBuilder<MasterFontFamily>();
            var filter = FilterDefinition<MasterFontFamily>.Empty;
            filter = builder.Eq(x => x.IsDefault, true);
            var found = await Collection.FindAsync(filter);
            var masterFontFamily = await found.FirstOrDefaultAsync();
            var config = new MapperConfiguration(cfg => { cfg.CreateMap<IMasterFontFamily, MasterFontFamily>(); });
            return config.CreateMapper().Map<MasterFontFamily, IMasterFontFamily>(masterFontFamily);
        }
        public async Task<IMasterFontFamily> SetDefaultMasterFontFamily(string masterFontFamilyName)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(masterFontFamilyName))
                    throw new ArgumentNullException(nameof(masterFontFamilyName));

                var masterFonts = await GetMasterFontFamily(masterFontFamilyName);
                if (!masterFonts.Any())
                    throw new ArgumentException("No master font found with name " + masterFontFamilyName);

                // Reset default font.
                var definition = new UpdateDefinitionBuilder<MasterFontFamily>()
                        .Set(u => u.IsDefault, false)
                        .Set(u => u.UpdatedOn, TenantTime.Now)
                        .Set(u => u.UpdatedBy, ExtractCurrentUser());

                await Collection.UpdateOneAsync(FindDefaultMasterFontFamily(), definition);

                // Set default font.
                var definition1 = new UpdateDefinitionBuilder<MasterFontFamily>()
                        .Set(u => u.IsDefault, true)
                        .Set(u => u.UpdatedOn, TenantTime.Now)
                        .Set(u => u.UpdatedBy, ExtractCurrentUser());

                await Collection.UpdateOneAsync(FindByMasterFontFamilyName(masterFontFamilyName), definition1);
                return await GetDefaultMasterFontFamily();
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        public async Task<List<IMasterFontFamily>> GetMasterFontFamily(string masterFontFamilyName = "")
        {
            var builder = new FilterDefinitionBuilder<MasterFontFamily>();
            var filter = FilterDefinition<MasterFontFamily>.Empty;
            if (!string.IsNullOrWhiteSpace(masterFontFamilyName))
            {
                filter = builder.Where(x => x.Name.ToLower() == masterFontFamilyName.ToLower());
            }
            var found = await Collection.FindAsync(filter);
            var masterFonts = await found.ToListAsync();
            var config = new MapperConfiguration(cfg => { cfg.CreateMap<List<IMasterFontFamily>, List<MasterFontFamily>>(); });
            return config.CreateMapper().Map<List<MasterFontFamily>, List<IMasterFontFamily>>(masterFonts);
        }
        public async Task<bool> AddMasterFontFamily(IMasterFontFamily masterFontFamily)
        {
            if (masterFontFamily == null)
                throw new ArgumentNullException(nameof(masterFontFamily));
            if (string.IsNullOrWhiteSpace(masterFontFamily.Name))
                throw new ArgumentNullException(nameof(masterFontFamily.Name));
            if (!HasSpecialChars(masterFontFamily.Name))
                throw new ArgumentException("Only alphanumeric and whitespace is allowed in the name.");
            if (string.IsNullOrWhiteSpace(masterFontFamily.Category))
                throw new ArgumentNullException(nameof(masterFontFamily.Category));
            if (string.IsNullOrWhiteSpace(masterFontFamily.Source))
                throw new ArgumentNullException(nameof(masterFontFamily.Source));
            if (string.IsNullOrWhiteSpace(masterFontFamily.SourceFontUrl))
                throw new ArgumentNullException(nameof(masterFontFamily.SourceFontUrl));
            if (masterFontFamily.FontWeight == null || (masterFontFamily.FontWeight.Count == 0))
                throw new ArgumentNullException(nameof(masterFontFamily.FontWeight));

            EnsureFontWeights(masterFontFamily.FontWeight);

            try
            {
                if (masterFontFamily.IsDefault)
                {
                    // Reset default font.
                    var definition = new UpdateDefinitionBuilder<MasterFontFamily>()
                            .Set(u => u.IsDefault, false)
                            .Set(u => u.UpdatedOn, TenantTime.Now)
                            .Set(u => u.UpdatedBy, ExtractCurrentUser());

                    await Collection.UpdateOneAsync(FindDefaultMasterFontFamily(), definition);
                }

                var masterFonts = await GetMasterFontFamily(masterFontFamily.Name);
                if (!masterFonts.Any())
                {
                    masterFontFamily.Id = ObjectId.GenerateNewId().ToString();

                    var record = new MasterFontFamily
                    {
                        Id = masterFontFamily.Id,
                        Name = masterFontFamily.Name,
                        Category = masterFontFamily.Category,
                        Source = masterFontFamily.Source,
                        SourceFontUrl = masterFontFamily.SourceFontUrl,
                        FontWeight = masterFontFamily.FontWeight,
                        Designers = masterFontFamily.Designers,
                        IsDefault = masterFontFamily.IsDefault,
                        CreatedBy = ExtractCurrentUser(),
                        CreatedOn = TenantTime.Now
                    };
                    await Collection.InsertOneAsync(record);
                    return true;
                }
                else
                {
                    var definition = new UpdateDefinitionBuilder<MasterFontFamily>()
                    .Set(u => u.Category, masterFontFamily.Category)
                    .Set(u => u.Source, masterFontFamily.Source)
                    .Set(u => u.SourceFontUrl, masterFontFamily.SourceFontUrl)
                    .Set(u => u.FontWeight, masterFontFamily.FontWeight)
                    .Set(u => u.Designers, masterFontFamily.Designers)
                    .Set(u => u.IsDefault, masterFontFamily.IsDefault)
                    .Set(u => u.UpdatedOn, TenantTime.Now)
                    .Set(u => u.UpdatedBy, ExtractCurrentUser());

                    await Collection.UpdateOneAsync(FindByMasterFontFamilyName(masterFontFamily.Name), definition);
                    return true;
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        private void EnsureFontWeights(List<string> fontWeights)
        {
            foreach (var fontWeight in fontWeights)
            {
                if (string.IsNullOrWhiteSpace(fontWeight))
                {
                    throw new ArgumentNullException(nameof(fontWeight));
                }
            }
        }

        private Expression<Func<MasterFontFamily, bool>> FindByMasterFontFamilyName(string masterFontFamilyName)
        {
            return x => x.Name.ToLower() == masterFontFamilyName.ToLower();
        }
        private Expression<Func<MasterFontFamily, bool>> FindDefaultMasterFontFamily()
        {
            return x => x.IsDefault == true;
        }
        private string ExtractCurrentUser()
        {
            var token = TokenHandler.Parse(TokenReader.Read());
            var username = token?.Subject;
            //if (string.IsNullOrWhiteSpace(token?.Subject))
            //    throw new ArgumentException("User is not authorized");
            return username;
        }

        private bool HasSpecialChars(string value)
        {
            Regex r = new Regex(@"^[a-zA-Z0-9\s]+$");
            if (r.IsMatch(value))
            {
                return true;
            }
            return false;
        }

        public async Task RemoveMasterFontFamilyBySource(string sourceName)
        {
            await Collection.DeleteManyAsync(x => x.Source == sourceName);
        }
    }
}