using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Persistence.Mongo;
using LendFoundry.Security.Tokens;
using LendFoundry.Tenant.Client;
using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using MongoDB.Bson.Serialization.Serializers;
using MongoDB.Driver;
using MongoDB.Driver.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Docitt.Theme
{
    public class ColorPalletRepository : MongoRepository<IColorPallet, ColorPallet>, IColorPalletRepository
    {
        static ColorPalletRepository()
        {
            BsonClassMap.RegisterClassMap<ColorPallet>(map =>
            {
                map.AutoMap();
                map.MapProperty(a => a.AppliedOn).SetSerializer(new NullableSerializer<DateTimeOffset>());
                map.MapProperty(a => a.UpdatedOn).SetSerializer(new NullableSerializer<DateTimeOffset>());
                var type = typeof(ColorPallet);
                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
                map.SetIsRootClass(true);
            });

            BsonClassMap.RegisterClassMap<Color>(map =>
            {
                map.AutoMap();
                var type = typeof(Color);
                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
            });
        }
        public ColorPalletRepository(ITenantService tenantService
                                    , IMongoConfiguration configuration
                                    , ITenantTime tenantTime
                                    , ITokenReader tokenReader
                                    , ITokenHandler tokenHandler) : base(tenantService, configuration, "colorpallets")
        {
            if (configuration == null)
                throw new ArgumentNullException(nameof(configuration));

            if (string.IsNullOrWhiteSpace(configuration.ConnectionString))
                throw new ArgumentException("Connection String cannot be empty");

            if (string.IsNullOrWhiteSpace(configuration.Database))
                throw new ArgumentException("Database name cannot be empty");

            TenantService = tenantService ?? throw new ArgumentNullException(nameof(tenantService));
            TenantTime = tenantTime ?? throw new ArgumentNullException(nameof(tenantTime));
            TokenReader = tokenReader ?? throw new ArgumentNullException(nameof(tokenReader));
            TokenHandler = tokenHandler ?? throw new ArgumentNullException(nameof(tokenHandler));
        }

        private ITenantService TenantService { get; }
        private ITenantTime TenantTime { get; }
        private ITokenReader TokenReader { get; }
        private ITokenHandler TokenHandler { get; }

        public async Task<IColorPallet> GetDefaultColorPallet()
        {
            return await Query.Where(x => x.IsDefault == true).FirstOrDefaultAsync();
        }
        public async Task<List<IColorPallet>> GetSystemDefinedColorPallet()
        {
            return await Query.Where(x => x.IsSystemDefined == true).ToListAsync();
        }
        public async Task<IColorPallet> GetCurrentColorPallet()
        {
            return await Query.Where(x => x.IsApplied == true).FirstOrDefaultAsync();
        }
        public async Task<List<IColorPallet>> GetColorPalletByName(string colorPalletName)
        {
            if (string.IsNullOrWhiteSpace(colorPalletName))
                throw new ArgumentNullException(nameof(colorPalletName));

            return await Query.Where(x => x.Name == colorPalletName.ToLower()).ToListAsync();
        }
        public async Task<List<IColorPallet>> GetAllColorPallet()
        {
            return await Query.ToListAsync();
        }
        public async Task<IColorPallet> AddColorPallet(IColorPallet colorPallet)
        {
            if (colorPallet == null)
                throw new ArgumentNullException(nameof(colorPallet));
            if (colorPallet.Name == null || string.IsNullOrWhiteSpace(colorPallet.Name))
                throw new ArgumentNullException(nameof(colorPallet.Name));
            if (HasSpecialChars(colorPallet.Name))
                throw new ArgumentException("Special characters are not allowed in name");
            if (colorPallet.Colors == null || colorPallet.Colors.Count == 0)
                throw new ArgumentNullException(nameof(colorPallet.Colors));

            EnsureColors(colorPallet.Colors);

            try
            {
                var result = Query.FirstOrDefault(x => x.Name.ToLower() == colorPallet.Name.ToLower());
                if (result == null)
                {
                    colorPallet.Id = ObjectId.GenerateNewId().ToString();

                    var record = new ColorPallet
                    {
                        Id = colorPallet.Id,
                        Name = colorPallet.Name,
                        Colors = colorPallet.Colors,
                        IsDefault = colorPallet.IsDefault,
                        IsApplied = colorPallet.IsApplied,
                        IsSystemDefined = colorPallet.IsSystemDefined,
                        CreatedBy = ExtractCurrentUser(),
                        CreatedOn = TenantTime.Now,
                        TenantId = TenantService.Current.Id
                    };
                    await Collection.InsertOneAsync(record);
                }
                else
                {
                    await Collection.UpdateOneAsync(Builders<IColorPallet>.Filter.Where(a => a.TenantId == TenantService.Current.Id
                                                            && a.Name.ToLower() == colorPallet.Name.ToLower()),
                                                    Builders<IColorPallet>.Update
                                                            .Set(a => a.Colors, colorPallet.Colors)
                                                            .Set(a => a.UpdatedOn, TenantTime.Now)
                                                            .Set(a => a.UpdatedBy, ExtractCurrentUser()));

                }
                return await Task.Run(() => Query.Where(x => x.Name.ToLower() == colorPallet.Name.ToLower()).FirstOrDefault());
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        private void EnsureColors(List<IColor> colors)
        {
            foreach (var color in colors)
            {
                if (string.IsNullOrWhiteSpace(color.Name))
                    throw new ArgumentNullException(nameof(color.Name));
                else if (string.IsNullOrWhiteSpace(color.Code))
                    throw new ArgumentNullException(nameof(color.Code));
            }
        }

        private bool HasSpecialChars(string value)
        {
            return value.Any(ch => !Char.IsLetterOrDigit(ch));
        }

        public async Task<bool> ApplyColorPallet(string colorPalletName)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(colorPalletName))
                    throw new ArgumentNullException(nameof(colorPalletName));

                var result = await Collection.UpdateOneAsync(Builders<IColorPallet>.Filter.Where(a => a.TenantId == TenantService.Current.Id
                                                            && a.Name.ToLower() == colorPalletName.ToLower()),
                                                    Builders<IColorPallet>.Update
                                                            .Set(a => a.IsApplied, true)
                                                            .Set(a => a.AppliedOn, TenantTime.Now)
                                                            .Set(a => a.AppliedBy, ExtractCurrentUser()));
                if (result.ModifiedCount == 1)
                {
                    var result1 = await Collection.UpdateOneAsync(Builders<IColorPallet>.Filter.Where(a => a.TenantId == TenantService.Current.Id
                                                            && a.Name.ToLower() != colorPalletName.ToLower()
                                                            && a.IsApplied == true),
                                                    Builders<IColorPallet>.Update
                                                            .Set(a => a.IsApplied, false)
                                                            .Set(a => a.AppliedBy, string.Empty)
                                                            .Set(a => a.AppliedOn, null)
                                                            .Set(a => a.UpdatedOn, TenantTime.Now)
                                                            .Set(a => a.UpdatedBy, ExtractCurrentUser()));
                }
                else
                {
                    throw new ArgumentException("No color pallet was found with the name " + colorPalletName);
                }
                return true;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        private string ExtractCurrentUser()
        {
            var token = TokenHandler.Parse(TokenReader.Read());
            var username = token?.Subject;
            //if (string.IsNullOrWhiteSpace(token?.Subject))
            //    throw new ArgumentException("User is not authorized");
            return username;
        }
    }
}