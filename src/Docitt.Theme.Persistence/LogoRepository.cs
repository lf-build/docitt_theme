﻿using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Persistence.Mongo;
using LendFoundry.Security.Tokens;
using LendFoundry.Tenant.Client;
using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using MongoDB.Bson.Serialization.Serializers;
using MongoDB.Driver;
using MongoDB.Driver.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Docitt.Theme
{
    public class LogoRepository : MongoRepository<ILogo, Logo>, ILogoRepository
    {
        static LogoRepository()
        {
            BsonClassMap.RegisterClassMap<Logo>(map =>
            {
                map.AutoMap();
                map.SetIgnoreExtraElements(true);
                map.UnmapMember(a => a.BadgeLogoUrl);
                map.UnmapMember(a => a.LogoUrl);
                map.UnmapMember(a => a.FavIconUrl);
                map.MapProperty(a => a.AppliedOn).SetSerializer(new NullableSerializer<DateTimeOffset>());
                var type = typeof(Logo);
                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
                map.SetIsRootClass(true);
            });
        }

        public LogoRepository(ITenantService tenantService
                                    , IMongoConfiguration configuration
                                    , ITenantTime tenantTime
                                    , ITokenReader tokenReader
                                    , ITokenHandler tokenHandler) : base(tenantService, configuration, "logos")
        {
            if (configuration == null)
                throw new ArgumentNullException(nameof(configuration));

            if (string.IsNullOrWhiteSpace(configuration.ConnectionString))
                throw new ArgumentException("Connection String cannot be empty");

            if (string.IsNullOrWhiteSpace(configuration.Database))
                throw new ArgumentException("Database name cannot be empty");

            TenantService = tenantService ?? throw new ArgumentNullException(nameof(tenantService));
            TenantTime = tenantTime ?? throw new ArgumentNullException(nameof(tenantTime));
            TokenReader = tokenReader ?? throw new ArgumentNullException(nameof(tokenReader));
            TokenHandler = tokenHandler ?? throw new ArgumentNullException(nameof(tokenHandler));
        }

        private ITenantService TenantService { get; }
        private ITenantTime TenantTime { get; }
        private ITokenReader TokenReader { get; }
        private ITokenHandler TokenHandler { get; }
        public async Task<ILogo> GetDefaultLogo()
        {
            return await Query.Where(x => x.IsDefault == true).FirstOrDefaultAsync();
        }
        public async Task<ILogo> GetCurrentLogo()
        {
            return await Query.Where(x => x.IsApplied == true).FirstOrDefaultAsync();
        }
        public async Task<ILogo> AddLogo(ILogo logo)
        {
            if (logo == null)
                throw new ArgumentNullException(nameof(logo));
            if (string.IsNullOrWhiteSpace(logo.BadgeLogoName))
                throw new ArgumentNullException(nameof(logo.BadgeLogoName));
            if (string.IsNullOrWhiteSpace(logo.LogoName))
                throw new ArgumentNullException(nameof(logo.LogoName));
            if (string.IsNullOrWhiteSpace(logo.FavIconName))
                throw new ArgumentNullException(nameof(logo.FavIconName));
            try
            {
                if (logo.IsApplied)
                {
                    var result = await Collection.UpdateOneAsync(Builders<ILogo>.Filter.Where(a => a.TenantId == TenantService.Current.Id
                                                            && a.IsApplied == true),
                                                     Builders<ILogo>.Update
                                                             .Set(a => a.IsApplied, false)
                                                             .Set(a => a.AppliedOn, null)
                                                             .Set(a => a.AppliedBy, string.Empty));
                }

                if (logo.IsDefault)
                {
                    var result = Query.FirstOrDefault(x => x.IsDefault == true);
                    if (result == null)
                    {
                        logo.Id = ObjectId.GenerateNewId().ToString();

                        var entry = new Logo
                        {
                            Id = logo.Id,
                            IsDefault = logo.IsDefault,
                            IsApplied = logo.IsApplied,
                            LogoName = logo.LogoName,
                            BadgeLogoName = logo.BadgeLogoName,
                            FavIconName = logo.FavIconName,
                            AppliedBy = ExtractCurrentUser(),
                            AppliedOn = TenantTime.Now,
                            TenantId = TenantService.Current.Id
                        };
                        await Collection.InsertOneAsync(entry);
                    }
                    else
                    {
                        var result1 = await Collection.UpdateOneAsync(Builders<ILogo>.Filter.Where(a => a.TenantId == TenantService.Current.Id
                                                                    && a.IsDefault == true),
                                                            Builders<ILogo>.Update
                                                                    .Set(a => a.LogoName, logo.LogoName)
                                                                    .Set(a => a.BadgeLogoName, logo.BadgeLogoName)
                                                                    .Set(a => a.FavIconName, logo.FavIconName)
                                                                    .Set(a => a.IsApplied, logo.IsApplied)
                                                                    .Set(a => a.AppliedBy, ExtractCurrentUser())
                                                                    .Set(a => a.AppliedOn, TenantTime.Now));
                    }

                    return await Task.Run(() => Query.Where(x => x.IsDefault == true).FirstOrDefault());
                }
                else
                {
                    var result = Query.FirstOrDefault(x => x.IsDefault == false);
                    if (result == null)
                    {
                        logo.Id = ObjectId.GenerateNewId().ToString();

                        var record = new Logo
                        {
                            Id = logo.Id,
                            IsDefault = logo.IsDefault,
                            IsApplied = logo.IsApplied,
                            LogoName = logo.LogoName,
                            BadgeLogoName = logo.LogoName,
                            FavIconName = logo.FavIconName,
                            AppliedBy = ExtractCurrentUser(),
                            AppliedOn = TenantTime.Now,
                            TenantId = TenantService.Current.Id
                        };
                        await Collection.InsertOneAsync(record);
                    }
                    else
                    {
                        var result1 = await Collection.UpdateOneAsync(Builders<ILogo>.Filter.Where(a => a.TenantId == TenantService.Current.Id
                                                                    && a.IsDefault == false),
                                                            Builders<ILogo>.Update
                                                                    .Set(a => a.LogoName, logo.LogoName)
                                                                    .Set(a => a.BadgeLogoName, logo.BadgeLogoName)
                                                                    .Set(a => a.FavIconName, logo.FavIconName)
                                                                    .Set(a => a.IsApplied, logo.IsApplied)
                                                                    .Set(a => a.AppliedBy, ExtractCurrentUser())
                                                                    .Set(a => a.AppliedOn, TenantTime.Now));
                    }
                    return await Task.Run(() => Query.Where(x => x.IsDefault == false).FirstOrDefault());
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public async Task<List<ILogo>> GetAllLogos()
        {
            return await Query.ToListAsync();
        }

        private string ExtractCurrentUser()
        {
            var token = TokenHandler.Parse(TokenReader.Read());
            var username = token?.Subject;
            //if (string.IsNullOrWhiteSpace(token?.Subject))
            //    throw new ArgumentException("User is not authorized");
            return username;
        }
    }
}
