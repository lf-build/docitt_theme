﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Services;
using Microsoft.AspNetCore.Mvc;

namespace Docitt.Theme.Api.Controllers
{
    /// <summary>
    /// Represent asset controller class.
    /// </summary>
    [Route("/asset")]
    public class AssetController : ExtendedController
    {
        /// <summary>
        /// Represents constructor
        /// </summary>
        /// <param name="logger"></param>
        /// <param name="assetService"></param>
        public AssetController(ILogger logger, IAssetService assetService)
        {
            Log = logger;
            AssetService = assetService;
        }

        /// <summary>
        /// Logger
        /// </summary>
        private ILogger Log { get; }

        /// <summary>
        /// AssetService
        /// </summary>
        private IAssetService AssetService { get; }

        /// <summary>
        /// Add Asset
        /// </summary>
        /// <param name="asset"></param>
        /// <returns></returns>
        [HttpPost]
        [ProducesResponseType(typeof(IAsset), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(ErrorResult), (int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> AddAsset([FromBody]Asset asset)
        {
            try
            {
                return Ok(await AssetService.AddAsset(asset));
            }
            catch (ArgumentException exception)
            {
                return ErrorResult.BadRequest(exception.Message);
            }
            catch (Exception exception)
            {
                return ErrorResult.BadRequest(exception.Message);
            }
        }

        /// <summary>
        /// Get default asset
        /// </summary>
        /// <returns></returns>
        [HttpGet("default")]
        [ProducesResponseType(typeof(IAsset), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(ErrorResult), (int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetDefaultAsset()
        {
            try
            {
                return Ok(await AssetService.GetDefaultAsset());
            }
            catch (Exception exception)
            {
                return ErrorResult.BadRequest(exception.Message);
            }
        }

        /// <summary>
        /// Get current asset
        /// </summary>
        /// <returns></returns>
        [HttpGet("current")]
        [ProducesResponseType(typeof(IAsset), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(ErrorResult), (int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetCurrentAsset()
        {
            try
            {
                return Ok(await AssetService.GetCurrentAsset());
            }
            catch (Exception exception)
            {
                return ErrorResult.BadRequest(exception.Message);
            }
        }

        /// <summary>
        /// Get all assets
        /// </summary>
        /// <returns></returns>
        [HttpGet("all")]
        [ProducesResponseType(typeof(List<IAsset>), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(ErrorResult), (int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetAllAssets()
        {
            try
            {
                return Ok(await AssetService.GetAllAssets());
            }
            catch (Exception exception)
            {
                return ErrorResult.BadRequest(exception.Message);
            }
        }
    }
}