﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Services;
using Microsoft.AspNetCore.Mvc;

namespace Docitt.Theme.Api.Controllers
{
    /// <summary>
    /// Represent logo controller class.
    /// </summary>
    [Route("/logo")]
    public class LogoController : ExtendedController
    {
        /// <summary>
        /// Represents constructor
        /// </summary>
        /// <param name="logger"></param>
        /// <param name="logoService"></param>
        public LogoController(ILogger logger, ILogoService logoService)
        {
            Log = logger;
            LogoService = logoService;
        }

        /// <summary>
        /// Logger
        /// </summary>
        private ILogger Log { get; }

        /// <summary>
        /// LogoService
        /// </summary>
        private ILogoService LogoService { get; }

        /// <summary>
        /// Add Logo
        /// </summary>
        /// <param name="logo"></param>
        /// <returns></returns>
        [HttpPost]
        [ProducesResponseType(typeof(ILogo), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(ErrorResult), (int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> AddLogo([FromBody]Logo logo)
        {
            try
            {
                return Ok(await LogoService.AddLogo(logo));
            }
            catch (ArgumentException exception)
            {
                return ErrorResult.BadRequest(exception.Message);
            }
            catch (Exception exception)
            {
                return ErrorResult.BadRequest(exception.Message);
            }
        }

        /// <summary>
        /// Get default logo
        /// </summary>
        /// <returns></returns>
        [HttpGet("default")]
        [ProducesResponseType(typeof(ILogo), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(ErrorResult), (int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetDefaultLogo()
        {
            try
            {
                return Ok(await LogoService.GetDefaultLogo());
            }
            catch (Exception exception)
            {
                return ErrorResult.BadRequest(exception.Message);
            }
        }

        /// <summary>
        /// Get current logo
        /// </summary>
        /// <returns></returns>
        [HttpGet("current")]
        [ProducesResponseType(typeof(ILogo), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(ErrorResult), (int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetCurrentLogo()
        {
            try
            {
                return Ok(await LogoService.GetCurrentLogo());
            }
            catch (Exception exception)
            {
                return ErrorResult.BadRequest(exception.Message);
            }
        }

        /// <summary>
        /// Get all logos
        /// </summary>
        /// <returns></returns>
        [HttpGet("all")]
        [ProducesResponseType(typeof(List<ILogo>), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(ErrorResult), (int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetAllLogos()
        {
            try
            {
                return Ok(await LogoService.GetAllLogos());
            }
            catch (Exception exception)
            {
                return ErrorResult.BadRequest(exception.Message);
            }
        }
    }
}