﻿using System;
using System.Net;
using System.Threading.Tasks;
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Services;
using Microsoft.AspNetCore.Mvc;

namespace Docitt.Theme.Api.Controllers
{
    /// <summary>
    /// Represent tenantfontfamily controller class.
    /// </summary>
    [Route("/tenantfontfamily")]
    public class TenantFontFamilyController : ExtendedController
    {
        /// <summary>
        /// Represents constructor
        /// </summary>
        /// <param name="logger"></param>
        /// <param name="tenantFontFamilyService"></param>
        public TenantFontFamilyController(ILogger logger, ITenantFontFamilyService tenantFontFamilyService)
        {
            Log = logger;
            TenantFontFamilyService = tenantFontFamilyService;
        }

        /// <summary>
        /// Logger
        /// </summary>
        private ILogger Log { get; }

        /// <summary>
        /// TenantFontFamilyService
        /// </summary>
        private ITenantFontFamilyService TenantFontFamilyService { get; }

        /// <summary>
        /// Add tenant font family
        /// </summary>
        /// <param name="tenantFontFamily"></param>
        /// <returns></returns>
        [HttpPost]
        [ProducesResponseType(typeof(ITenantFontFamily), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(ErrorResult), (int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> ApplyTenantFontFamily([FromBody]TenantFontFamily tenantFontFamily)
        {
            try
            {
                return Ok(await TenantFontFamilyService.ApplyTenantFontFamily(tenantFontFamily));
            }
            catch (ArgumentException exception)
            {
                return ErrorResult.BadRequest(exception.Message);
            }
            catch (Exception exception)
            {
                return ErrorResult.BadRequest(exception.Message);
            }
        }

        /// <summary>
        /// Get tenant font family
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [ProducesResponseType(typeof(ITenantFontFamily), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(ErrorResult), (int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetTenantFontFamily()
        {
            try
            {
                return Ok(await TenantFontFamilyService.GetTenantFontFamily());
            }
            catch (Exception exception)
            {
                return ErrorResult.BadRequest(exception.Message);
            }
        }
    }
}