﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Services;
using Microsoft.AspNetCore.Mvc;

namespace Docitt.Theme.Api.Controllers
{
    /// <summary>
    /// Represent iconstyle controller class.
    /// </summary>
    [Route("/iconstyle")]
    public class IconStyleController : ExtendedController
    {
        /// <summary>
        /// Represents constructor
        /// </summary>
        /// <param name="logger"></param>
        /// <param name="iconStyleService"></param>
        public IconStyleController(ILogger logger, IIconStyleService iconStyleService)
        {
            Log = logger;
            IconStyleService = iconStyleService;
        }

        /// <summary>
        /// Logger
        /// </summary>
        private ILogger Log { get; }

        /// <summary>
        /// Icon style service
        /// </summary>
        private IIconStyleService IconStyleService { get; }

        /// <summary>
        /// Add icon style
        /// </summary>
        /// <param name="iconStyle"></param>
        /// <returns></returns>
        [HttpPost]
        [ProducesResponseType(typeof(IIconStyle), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(ErrorResult), (int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> AddIconStyle([FromBody]IconStyle iconStyle)
        {
            try
            {
                return Ok(await IconStyleService.AddIconStyle(iconStyle));
            }
            catch (ArgumentException exception)
            {
                return ErrorResult.BadRequest(exception.Message);
            }
            catch (Exception exception)
            {
                return ErrorResult.BadRequest(exception.Message);
            }
        }

        /// <summary>
        /// Get default icon style
        /// </summary>
        /// <returns></returns>
        [HttpGet("default")]
        [ProducesResponseType(typeof(IIconStyle), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(ErrorResult), (int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetDefaultIconStyle()
        {
            try
            {
                return Ok(await IconStyleService.GetDefaultIconStyle());
            }
            catch (Exception exception)
            {
                return ErrorResult.BadRequest(exception.Message);
            }
        }

        /// <summary>
        /// Get current icon style
        /// </summary>
        /// <returns></returns>
        [HttpGet("current")]
        [ProducesResponseType(typeof(IIconStyle), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(ErrorResult), (int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetCurrentIconStyle()
        {
            try
            {
                return Ok(await IconStyleService.GetCurrentIconStyle());
            }
            catch (Exception exception)
            {
                return ErrorResult.BadRequest(exception.Message);
            }
        }

        /// <summary>
        /// Get all icon styles
        /// </summary>
        /// <returns></returns>
        [HttpGet("all")]
        [ProducesResponseType(typeof(List<IIconStyle>), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(ErrorResult), (int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetAllIconStyle()
        {
            try
            {
                return Ok(await IconStyleService.GetAllIconStyle());
            }
            catch (Exception exception)
            {
                return ErrorResult.BadRequest(exception.Message);
            }
        }
        /// <summary>
        /// Apply icon style
        /// </summary>
        /// <param name="iconStyleName"></param>
        /// <returns></returns>
        [HttpPut("apply/{iconStyleName}")]
        [ProducesResponseType(typeof(bool), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(ErrorResult), (int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> ApplyIconStyle(string iconStyleName)
        {
            try
            {
                return Ok(await IconStyleService.ApplyIconStyle(iconStyleName));
            }
            catch (Exception exception)
            {
                return ErrorResult.BadRequest(exception.Message);
            }
        }
    }
}