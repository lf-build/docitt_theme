﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Services;
using Microsoft.AspNetCore.Mvc;

namespace Docitt.Theme.Api.Controllers
{
    /// <summary>
    /// Represent colorpallet controller class.
    /// </summary>
    [Route("/colorpallet")]
    public class ColorPalletController : ExtendedController
    {
        /// <summary>
        /// Represents constructor
        /// </summary>
        /// <param name="logger"></param>
        /// <param name="colorPalletService"></param>
        public ColorPalletController(ILogger logger, IColorPalletService colorPalletService)
        {
            Log = logger;
            ColorPalletService = colorPalletService;
        }

        /// <summary>
        /// Logger
        /// </summary>
        private ILogger Log { get; }

        /// <summary>
        /// ColorPalletService
        /// </summary>
        private IColorPalletService ColorPalletService { get; }

        /// <summary>
        /// Add ColorPallet
        /// </summary>
        /// <param name="colorPallet"></param>
        /// <returns></returns>
        [HttpPost]
        [ProducesResponseType(typeof(IColorPallet), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(ErrorResult), (int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> AddColorPallet([FromBody]ColorPallet colorPallet)
        {
            try
            {
                return Ok(await ColorPalletService.AddColorPallet(colorPallet));
            }
            catch (ArgumentException exception)
            {
                return ErrorResult.BadRequest(exception.Message);
            }
            catch (Exception exception)
            {
                return ErrorResult.BadRequest(exception.Message);
            }
        }

        /// <summary>
        /// Get default color pallet
        /// </summary>
        /// <returns></returns>
        [HttpGet("default")]
        [ProducesResponseType(typeof(IColorPallet), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(ErrorResult), (int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetDefaultColorPallet()
        {
            try
            {
                return Ok(await ColorPalletService.GetDefaultColorPallet());
            }
            catch (Exception exception)
            {
                return ErrorResult.BadRequest(exception.Message);
            }
        }

        /// <summary>
        /// Get current color pallet
        /// </summary>
        /// <returns></returns>
        [HttpGet("current")]
        [ProducesResponseType(typeof(IColorPallet), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(ErrorResult), (int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetCurrentColorPallet()
        {
            try
            {
                return Ok(await ColorPalletService.GetCurrentColorPallet());
            }
            catch (Exception exception)
            {
                return ErrorResult.BadRequest(exception.Message);
            }
        }

        /// <summary>
        /// Get system defined color pallet
        /// </summary>
        /// <returns></returns>
        [HttpGet("systemdefined")]
        [ProducesResponseType(typeof(List<IColorPallet>), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(ErrorResult), (int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetSystemDefinedColorPallet()
        {
            try
            {
                return Ok(await ColorPalletService.GetSystemDefinedColorPallet());
            }
            catch (Exception exception)
            {
                return ErrorResult.BadRequest(exception.Message);
            }
        }
        /// <summary>
        /// Get all color pallets
        /// </summary>
        /// <returns></returns>
        [HttpGet("all")]
        [ProducesResponseType(typeof(List<IColorPallet>), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(ErrorResult), (int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetAllColorPallet()
        {
            try
            {
                return Ok(await ColorPalletService.GetAllColorPallet());
            }
            catch (Exception exception)
            {
                return ErrorResult.BadRequest(exception.Message);
            }
        }
        /// <summary>
        /// Apply color pallet
        /// </summary>
        /// <param name="colorPalletName"></param>
        /// <returns></returns>
        [HttpPut("apply/{colorPalletName}")]
        [ProducesResponseType(typeof(bool), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(ErrorResult), (int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> ApplyColorPallet(string colorPalletName)
        {
            try
            {
                return Ok(await ColorPalletService.ApplyColorPallet(colorPalletName));
            }
            catch (Exception exception)
            {
                return ErrorResult.BadRequest(exception.Message);
            }
        }
    }
}