﻿using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Services;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Net;
using System.Threading.Tasks;

namespace Docitt.Theme.Api.Controllers
{
    /// <summary>
    /// Represent theme controller class.
    /// </summary>
    [Route("/theme")]
    public class ThemeController : ExtendedController
    {
        /// <summary>
        /// Represents constructor
        /// </summary>
        /// <param name="logger"></param>
        /// <param name="cssGenerator"></param>
        public ThemeController(ILogger logger, ICSSGenerator cssGenerator)
        {
            Log = logger;
            CSSGenerator = cssGenerator;
        }

        /// <summary>
        /// Logger
        /// </summary>
        private ILogger Log { get; }

        /// <summary>
        /// CSSGenerator
        /// </summary>
        private ICSSGenerator CSSGenerator { get; }

        /// <summary>
        /// Generate CSS
        /// </summary>
        /// <returns>bool</returns>
        [HttpPost("generate/css")]
        [ProducesResponseType(typeof(bool), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(ErrorResult), (int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GenerateCSS()
        {
            try
            {
                return Ok(await CSSGenerator.GenerateCSS());
            }
            catch (Exception exception)
            {
                return ErrorResult.BadRequest(exception.Message);
            }
        }
    }
}
