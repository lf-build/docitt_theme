﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Services;
using Microsoft.AspNetCore.Mvc;

namespace Docitt.Theme.Api.Controllers
{
    /// <summary>
    /// Represent masterfontfamily controller class.
    /// </summary>
    [Route("/masterfontfamily")]
    public class MasterFontFamilyController : ExtendedController
    {
        /// <summary>
        /// Represents constructor
        /// </summary>
        /// <param name="logger"></param>
        /// <param name="masterFontFamilyService"></param>
        public MasterFontFamilyController(ILogger logger, IMasterFontFamilyService masterFontFamilyService)
        {
            Log = logger;
            MasterFontFamilyService = masterFontFamilyService;
        }

        /// <summary>
        /// Logger
        /// </summary>
        private ILogger Log { get; }

        /// <summary>
        /// MasterFontFamilyService
        /// </summary>
        private IMasterFontFamilyService MasterFontFamilyService { get; }

        /// <summary>
        /// Add master font family
        /// </summary>
        /// <param name="masterFontFamily"></param>
        /// <returns></returns>
        [HttpPost]
        [ProducesResponseType(typeof(IMasterFontFamily), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(ErrorResult), (int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> AddMasterFontFamily([FromBody]MasterFontFamily masterFontFamily)
        {
            try
            {
                return Ok(await MasterFontFamilyService.AddMasterFontFamily(masterFontFamily));
            }
            catch (ArgumentException exception)
            {
                return ErrorResult.BadRequest(exception.Message);
            }
            catch (Exception exception)
            {
                return ErrorResult.BadRequest(exception.Message);
            }
        }

        /// <summary>
        /// Get default master font family
        /// </summary>
        /// <returns></returns>
        [HttpGet("default")]
        [ProducesResponseType(typeof(IMasterFontFamily), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(ErrorResult), (int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetDefaultMasterFontFamily()
        {
            try
            {
                return Ok(await MasterFontFamilyService.GetDefaultMasterFontFamily());
            }
            catch (Exception exception)
            {
                return ErrorResult.BadRequest(exception.Message);
            }
        }

        /// <summary>
        /// Set default master font family by name
        /// </summary>
        /// <param name="masterfontfamilyname"></param>
        /// <returns></returns>
        [HttpPut("default/{masterfontfamilyname}")]
        [ProducesResponseType(typeof(List<IMasterFontFamily>), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(ErrorResult), (int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> SetDefaultMasterFontFamily(string masterfontfamilyname)
        {
            try
            {
                return Ok(await MasterFontFamilyService.SetDefaultMasterFontFamily(masterfontfamilyname));
            }
            catch (Exception exception)
            {
                return ErrorResult.BadRequest(exception.Message);
            }
        }

        /// <summary>
        /// Get all master font families
        /// </summary>
        /// <returns></returns>
        [HttpGet("all")]
        [ProducesResponseType(typeof(List<IMasterFontFamily>), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(ErrorResult), (int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetMasterFontFamily()
        {
            try
            {
                return Ok(await MasterFontFamilyService.GetMasterFontFamily());
            }
            catch (Exception exception)
            {
                return ErrorResult.BadRequest(exception.Message);
            }
        }

        /// <summary>
        /// Get master font family by name
        /// </summary>
        /// <param name="masterfontfamilyname"></param>
        /// <returns></returns>
        [HttpGet("all/{masterfontfamilyname}")]
        [ProducesResponseType(typeof(List<IMasterFontFamily>), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(ErrorResult), (int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetMasterFontFamily(string masterfontfamilyname)
        {
            try
            {
                return Ok(await MasterFontFamilyService.GetMasterFontFamily(masterfontfamilyname));
            }
            catch (Exception exception)
            {
                return ErrorResult.BadRequest(exception.Message);
            }
        }

        /// <summary>
        /// Import google fonts
        /// </summary>
        /// <returns></returns>
        [HttpPost("import/googlefonts")]
        [ProducesResponseType(typeof(bool), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(ErrorResult), (int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> ImportGoogleFont()
        {
            try
            {
                return Ok(await MasterFontFamilyService.ImportGoogleFontFamily());
            }
            catch (ArgumentException exception)
            {
                return ErrorResult.BadRequest(exception.Message);
            }
            catch (Exception exception)
            {
                return ErrorResult.BadRequest(exception.Message);
            }
        }
    }
}