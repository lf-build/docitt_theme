﻿using LendFoundry.Configuration.Client;
using LendFoundry.EventHub.Client;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Persistence.Mongo;
using LendFoundry.Foundation.Services;
using LendFoundry.Security.Tokens;
using LendFoundry.Tenant.Client;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.PlatformAbstractions;
using Swashbuckle.AspNetCore.Swagger;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Microsoft.AspNetCore.Http;
using System.IO;
using System.Collections.Generic;
using LendFoundry.Configuration;
using LendFoundry.Foundation.ServiceDependencyResolver;
using LendFoundry.AssetManager.Client;

namespace Docitt.Theme.Api
{
    internal class Startup
    {
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("docs", new Info
                {
                    Version = PlatformServices.Default.Application.ApplicationVersion,
                    Title = "DocittTheme"
                });
                c.AddSecurityDefinition("Bearer", new ApiKeyScheme()
                {
                    Type = "apiKey",
                    Name = "Authorization",
                    Description = "For accessing the API a valid JWT token must be passed in all the queries in the 'Authorization' header. The syntax used in the 'Authorization' header should be Bearer xxxxx.yyyyyy.zzzz",
                    In = "header"
                });
                c.AddSecurityRequirement(new Dictionary<string, IEnumerable<string>>
                {
                    { "Bearer", new string[] { } }
                });
                c.TagActionsBy(api => "Docitt Theme");

                c.DescribeAllEnumsAsStrings();
                c.IgnoreObsoleteProperties();
                c.DescribeStringEnumsInCamelCase();
                c.IgnoreObsoleteActions();
                var basePath = PlatformServices.Default.Application.ApplicationBasePath;
                var xmlPath = Path.Combine(basePath, "Docitt.Theme.Api.xml");
                c.IncludeXmlComments(xmlPath);
            });
            services.TryAddSingleton<IHttpContextAccessor, HttpContextAccessor>();

            services.AddTenantTime();
            services.AddTokenHandler();
            services.AddHttpServiceLogging(Settings.ServiceName);

            services.AddEventHub(Settings.ServiceName);
            services.AddTenantService();
            services.AddAssetManager();

            services.AddConfigurationService<Configuration>(Settings.ServiceName);
            services.AddTransient<IConfiguration>(provider => provider.GetRequiredService<IConfigurationService<Configuration>>().Get());
            services.AddMongoConfiguration(Settings.ServiceName);
            services.AddDependencyServiceUriResolver<Configuration>(Settings.ServiceName);
            services.AddTransient<IColorPalletRepository, ColorPalletRepository>();
            services.AddTransient<IColorPalletService, ColorPalletService>();
            services.AddTransient<ILogoRepository, LogoRepository>();
            services.AddTransient<ILogoService, LogoService>();
            services.AddTransient<IAssetRepository, AssetRepository>();
            services.AddTransient<IAssetService, AssetService>();
            services.AddTransient<IMasterFontFamilyRepository, MasterFontFamilyRepository>();
            services.AddTransient<IMasterFontFamilyService, MasterFontFamilyService>();
            services.AddTransient<ITenantFontFamilyRepository, TenantFontFamilyRepository>();
            services.AddTransient<ITenantFontFamilyService, TenantFontFamilyService>();
            services.AddTransient<IIconStyleRepository, IconStyleRepository>();
            services.AddTransient<IIconStyleService, IconStyleService>();
            services.AddTransient<ICSSGenerator, CSSGenerator>();

            services.AddMvc().AddLendFoundryJsonOptions();
            services.AddCors();

        }
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            app.UseHealthCheck();
            app.UseCors(env);
            // Enable middleware to serve generated Swagger as a JSON endpoint.
            // Enable middleware to serve swagger-ui (HTML, JS, CSS etc.), specifying the Swagger JSON endpoint.
            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/docs/swagger.json", "Docitt Theme Service");
            });
            app.UseErrorHandling();
            app.UseRequestLogging();
            app.UseMvc();
            app.UseConfigurationCacheDependency();
        }
    }
}