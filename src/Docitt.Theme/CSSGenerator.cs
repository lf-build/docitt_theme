﻿using LendFoundry.AssetManager;
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Services;
using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Docitt.Theme
{
    public class CSSGenerator : ICSSGenerator
    {
        private ILogger Logger { get; }
        private IAssetManagerService AssetManagerService { get; }
        private IColorPalletService ColorPalletService { get; }
        private IIconStyleService IconStyleService { get; }
        private ITenantFontFamilyService TenantFontFamilyService { get; }
        private ILogoService LogoService { get; }
        private IAssetService AssetService { get; }
        private IConfiguration Configuration { get; }

        public CSSGenerator(ILogger logger, IAssetManagerService assetManagerService, IColorPalletService colorPalletService,
                            IIconStyleService iconStyleService, ITenantFontFamilyService tenantFontFamilyService, IConfiguration configuration,
                            ILogoService logoService, IAssetService assetService)
        {
            Logger = logger;
            AssetManagerService = assetManagerService;
            ColorPalletService = colorPalletService;
            IconStyleService = iconStyleService;
            Configuration = configuration;
            TenantFontFamilyService = tenantFontFamilyService;
            LogoService = logoService;
            AssetService = assetService;
            if (string.IsNullOrWhiteSpace(Configuration.CssCompilerCommand))
                throw new ArgumentException($"{nameof(Configuration.CssCompilerCommand)} is required");
            if (string.IsNullOrWhiteSpace(Configuration.CssCompilerArgs))
                throw new ArgumentException($"{nameof(Configuration.CssCompilerArgs)} is required");
            if (string.IsNullOrWhiteSpace(Configuration.CssMinifierCommand))
                throw new ArgumentException($"{nameof(Configuration.CssMinifierCommand)} is required");
            if (string.IsNullOrWhiteSpace(Configuration.CssMinifierArgs))
                throw new ArgumentException($"{nameof(Configuration.CssMinifierArgs)} is required");
        }

        private const int TimeoutInMilliseconds = 60 * 1000 * 5;

        public async Task<bool> GenerateCSS()
        {
            Logger.Debug("Started GenerateCSS.");
            try
            {
                if (!Directory.Exists(GetTemporaryWorkingDirectory()))
                {
                    Directory.CreateDirectory(GetTemporaryWorkingDirectory());
                }

                var lessList = await AssetManagerService.GetAll("theme", "less");

                #region Generate CSS
                if (lessList != null && lessList.Count() > 0)
                {
                    var tenantFontFamily = await TenantFontFamilyService.GetTenantFontFamily();
                    var logoDetails = await LogoService.GetCurrentLogo();
                    var assetDetails = await AssetService.GetCurrentAsset();
                    foreach (var item in lessList)
                    {
                        WebClient webClient = new WebClient();
                        string data = webClient.DownloadString(item.Url);

                        #region Replace colors and fonts url
                        if (item.FileName.Equals("style-guide.less"))
                        {
                            var colorPallet = await ColorPalletService.GetCurrentColorPallet();
                            if (colorPallet != null && colorPallet.Colors.Count > 0)
                            {
                                foreach (var item1 in colorPallet.Colors)
                                {
                                    string name = "#" + item1.Name + "#";
                                    data = data.Replace(name, item1.Code);
                                }
                            }
                            else
                            {
                                throw new NotFoundException("No colors were found.");
                            }

                            if (tenantFontFamily != null)
                            {
                                string fontUrl = tenantFontFamily.SourceFontUrl;
                                if (tenantFontFamily.FontWeight != null && tenantFontFamily.FontWeight.Count > 0)
                                {
                                    fontUrl = fontUrl + ":";
                                    foreach (var fontWeight in tenantFontFamily.FontWeight)
                                    {
                                        if (Configuration.RequiredFontWeight.Contains(fontWeight))
                                        {
                                            fontUrl = fontUrl + fontWeight + ",";
                                        }
                                    }
                                }
                                data = data.Replace("#FontUrl#", fontUrl);
                                // As discussed wth Swaraj FontFamily is in "style-guide.less"
                                if (tenantFontFamily != null)
                                {
                                    data = data.Replace("#FontFamily#", tenantFontFamily.Name);
                                }
                            }
                            else
                            {
                                throw new NotFoundException("No tenant fonts were found.");
                            }
                        }
                        #endregion
                        #region Replace font family
                        else if (item.FileName.ToLower().Equals("custom.less"))
                        {
                            data = data.Replace("#AssetBaseUrl#", Configuration.AssetBaseURL);
                            if (tenantFontFamily != null)
                            {
                                data = data.Replace("#FontFamily#", tenantFontFamily.Name);
                            }
                            else
                            {
                                throw new NotFoundException("No tenant fonts were found.");
                            }
                            #region Replace Logo and badgelogo.
                            if (logoDetails != null)
                            {
                                data = data.Replace("#LogoName#", logoDetails.LogoUrl);
                                data = data.Replace("#BadgeLogoName#", logoDetails.BadgeLogoUrl);
                            }
                            #endregion
                            #region Replace Assets
                            if (assetDetails != null)
                            {
                                data = data.Replace("#WelcomScreenImage#", assetDetails.WelcomScreenImageUrl);
                                data = data.Replace("#BackgroundImage#", assetDetails.WelcomeScreenBackgroundImageUrl);
                                data = data.Replace("#AlternateUserLogo#", assetDetails.AlternateUserLogoUrl);
                            }
                            #endregion
                        }
                        #endregion
                        #region Replace icons url
                        else if (item.FileName.ToLower().Equals("font-icons.less"))
                        {
                            var iconStyle = await IconStyleService.GetCurrentIconStyle();
                            if (iconStyle != null)
                            {
                                foreach (var iconSourceUrl in iconStyle.IconSourceUrl)
                                {
                                    string extension = Path.GetExtension(iconSourceUrl);
                                    if (extension.ToLower().Equals(".eot"))
                                    {
                                        data = data.Replace("#EotFontUrl#", iconSourceUrl);
                                    }
                                    else if (extension.ToLower().Equals(".svg"))
                                    {
                                        data = data.Replace("#SvgFontUrl#", iconSourceUrl);
                                    }
                                    else if (extension.ToLower().Equals(".ttf"))
                                    {
                                        data = data.Replace("#TtfFontUrl#", iconSourceUrl);
                                    }
                                    else if (extension.ToLower().Equals(".woff"))
                                    {
                                        data = data.Replace("#WoffFontUrl#", iconSourceUrl);
                                    }
                                }
                            }
                            else
                            {
                                throw new NotFoundException("No icons were found.");
                            }
                        }
                        #endregion

                        File.WriteAllText(GetTemporaryWorkingDirectory() + item.FileName, data);
                    }
                    var arguments =
                               new StringBuilder(
                                   Environment.OSVersion.Platform == PlatformID.Unix
                                       ? $" -c \"{GetBasePath(Configuration.CssCompilerCommand)} {Configuration.CssCompilerArgs}\" "
                                       : $"{Configuration.CssCompilerArgs}"
                                   ).ToString();

                    Logger.Debug("CSS compiler arguments are " + arguments);
                    var exitCode = await RunProcessAsync(GetBasePathName(Configuration.CssCompilerCommand), arguments);
                    if (exitCode > 0)
                    {
                        Logger.Error($"CSS process exited with exit code {exitCode}");
                        return false;
                    }
                }
                else
                {
                    throw new NotFoundException("No less files found.");
                }
                #endregion

                #region Minify CSS
                string[] cssFiles = Directory.GetFiles(GetTemporaryWorkingDirectory(), "*.css");
                if (cssFiles != null && cssFiles.Length > 0)
                {
                    var arguments =
                               new StringBuilder(
                                   Environment.OSVersion.Platform == PlatformID.Unix
                                       ? $" -c \"{GetBasePath(Configuration.CssMinifierCommand)} {Configuration.CssMinifierArgs}\" "
                                       : $"{Configuration.CssCompilerArgs}"
                                   ).ToString();

                    Logger.Debug("Minify css arguments are " + arguments);
                    var exitCode = await RunProcessAsync(GetBasePathName(Configuration.CssMinifierCommand), arguments);
                    if (exitCode > 0)
                    {
                        Logger.Error($"Minify process exited with exit code {exitCode}");
                        return false;
                    }
                }
                else
                {
                    Logger.Error("No css file found");
                    return false;
                }
                #endregion

                #region Upload Minify CSS to folder
                string[] minifyCssFiles = Directory.GetFiles(GetTemporaryWorkingDirectory(), "*.min.css");
                if (minifyCssFiles != null && minifyCssFiles.Length > 0)
                {
                    string filePath = minifyCssFiles[0];
                    string fileName = Path.GetFileName(filePath);

                    var minifyCSS = await AssetManagerService.Get(Configuration.EntityTypeForAssets, "css", fileName);
                    using (FileStream fileStream = new FileStream(filePath, FileMode.Open, FileAccess.Read))
                    {
                        if (minifyCSS == null)
                        {
                            var response = await AssetManagerService.Create(fileStream, Configuration.EntityTypeForAssets, "css", fileName);
                            if (response == null)
                            {
                                Logger.Warn("Not able to upload minify css");
                            }
                        }
                        else
                        {
                            var response = await AssetManagerService.Replace(fileStream, Configuration.EntityTypeForAssets, "css", fileName);
                            if (response == null)
                            {
                                Logger.Warn("Not able to replace minify css");
                            }
                        }
                    }
                    #region Delete all files from working directory
                    ClearFolder(GetTemporaryWorkingDirectory());
                    #endregion
                }
                else
                {
                    Logger.Error("No minify css file found");
                    return false;
                }
                #endregion
            }
            catch (Exception ex)
            {
                Logger.Error("Error occured while GenerateCSS", ex);
                throw;
            }
            Logger.Debug("Ended GenerateCSS.");
            return true;
        }

        private async Task<int> RunProcessAsync(string fileName, string args)
        {
            ProcessProxy processProxy = new ProcessProxy();

            processProxy.StartInfo = new ProcessStartInfo
            {
                FileName = fileName,
                Arguments = args,
                UseShellExecute = false,
                CreateNoWindow = true,
                RedirectStandardOutput = true,
                RedirectStandardError = true,
                RedirectStandardInput = true,
                WorkingDirectory = GetTemporaryWorkingDirectory()
            };

            processProxy.EnableRaisingEvents = true;

            Logger.Debug($"Running process {fileName} with args {args}");

            var exitCode = await RunProcessAsync(processProxy);

            try
            {
                if (!processProxy.HasExited)
                {
                    processProxy.Kill();
                    throw new InvalidOperationException(
                        $"Error in exiting the {processProxy} while executing. Process {fileName} with args {args} is killed ");
                }
            }
            catch (Exception ex)
            {
                Logger.Error("HasExited return error", ex, new { fileName, args });
            }

            return exitCode;
        }

        private Task<int> RunProcessAsync(ProcessProxy processProxy)
        {
            return Task.Run(() =>
            {
                var started = processProxy.Start();

                if (!started)
                {
                    throw new InvalidOperationException($"Could not start process {processProxy}");
                }

                if (processProxy.WaitForExit(TimeoutInMilliseconds))
                {
                    return processProxy.ExitCode;
                }
                else
                {
                    return -999;
                }

            });
        }

        public Func<string> GetTemporaryWorkingDirectory
        {
            get
            {
                return () => string.IsNullOrWhiteSpace(Settings.TemporaryFolder) ? "/temp/lf-theme/" : Settings.TemporaryFolder;
            }
        }

        private void ClearFolder(string FolderName)
        {
            DirectoryInfo dir = new DirectoryInfo(FolderName);
            foreach (FileInfo fi in dir.GetFiles())
            {
                fi.Delete();
            }
            foreach (DirectoryInfo di in dir.GetDirectories())
            {
                ClearFolder(di.FullName);
                di.Delete();
            }
        }

        private string GetBasePath(string fileName)
        {
            return Path.Combine(Settings.BasePath, fileName);
        }

        private string GetBasePathName(string fileName)
        {
            switch (Environment.OSVersion.Platform)
            {
                case PlatformID.Unix:
                case PlatformID.MacOSX:
                    return "/bin/sh";
                default:
                    return Path.Combine(Settings.BasePath, fileName);
            }
        }
    }
}
