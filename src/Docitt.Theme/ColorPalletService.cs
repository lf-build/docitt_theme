using LendFoundry.Foundation.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Docitt.Theme
{
    public class ColorPalletService : IColorPalletService
    {
        public ColorPalletService(
            ILogger logger,
            IColorPalletRepository colorPalletRepository)
        {
            Log = logger;
            ColorPalletRepository = colorPalletRepository;
        }

        private ILogger Log { get; }

        private IColorPalletRepository ColorPalletRepository { get; }

        public async Task<IColorPallet> AddColorPallet(IColorPallet colorPallet)
        {
            Log.Debug("Started AddColorPallet Service.");
            var result = await ColorPalletRepository.AddColorPallet(colorPallet);
            Log.Debug("Ended AddColorPallet Service.");
            return result;
        }

        public async Task<IColorPallet> GetDefaultColorPallet()
        {
            Log.Debug("Started GetDefaultColorPallet Service");
            var result = await ColorPalletRepository.GetDefaultColorPallet();
            Log.Debug("Ended GetDefaultColorPallet Servoce.");
            return result;
        }

        public async Task<List<IColorPallet>> GetSystemDefinedColorPallet()
        {
            Log.Debug("Started GetSystemDefinedColorPallet Service");
            var result = await ColorPalletRepository.GetSystemDefinedColorPallet();
            Log.Debug("Ended GetSystemDefinedColorPallet Servoce.");
            return result;
        }

        public async Task<IColorPallet> GetCurrentColorPallet()
        {
            Log.Debug("Started GetCurrentColorPallet Service");
            var result = await ColorPalletRepository.GetCurrentColorPallet();
            Log.Debug("Ended GetCurrentColorPallet Servoce.");
            return result;
        }

        public async Task<List<IColorPallet>> GetAllColorPallet()
        {
            Log.Debug("Started GetAllColorPallet Service");
            var result = await ColorPalletRepository.GetAllColorPallet();
            Log.Debug("Ended GetAllColorPallet Servoce.");
            return result;
        }

        public async Task<bool> ApplyColorPallet(string colorPalletName)
        {
            Log.Debug("Started ApplyColorPallet Service");
            var result = await ColorPalletRepository.ApplyColorPallet(colorPalletName);
            Log.Debug("Ended ApplyColorPallet Servoce.");
            return result;
        }
    }
}