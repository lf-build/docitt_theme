using LendFoundry.Foundation.Logging;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace Docitt.Theme
{
    public class MasterFontFamilyService : IMasterFontFamilyService
    {
        public MasterFontFamilyService(
            ILogger logger,
            IConfiguration configuration,
            IMasterFontFamilyRepository masterFontRepository)
        {
            Log = logger;
            MasterFontRepository = masterFontRepository;
            Configuration = configuration;
        }

        private ILogger Log { get; }

        private IMasterFontFamilyRepository MasterFontRepository { get; }
        private IConfiguration Configuration { get; }

        public async Task<bool> AddMasterFontFamily(IMasterFontFamily masterFontFamily)
        {
            Log.Debug("Started AddMasterFontFamily Service.");
            var result = await MasterFontRepository.AddMasterFontFamily(masterFontFamily);
            Log.Debug("Ended AddMasterFontFamily Service.");
            return result;
        }

        public async Task<IMasterFontFamily> GetDefaultMasterFontFamily()
        {
            Log.Debug("Started GetDefaultMasterFontFamily Service");
            var result = await MasterFontRepository.GetDefaultMasterFontFamily();
            Log.Debug("Ended GetDefaultMasterFontFamily Service.");
            return result;
        }
        public async Task<IMasterFontFamily> SetDefaultMasterFontFamily(string masterFontFamilyName)
        {
            Log.Debug("Started SetDefaultMasterFontFamily Service");
            var result = await MasterFontRepository.SetDefaultMasterFontFamily(masterFontFamilyName);
            Log.Debug("Ended SetDefaultMasterFontFamily Service.");
            return result;
        }

        public async Task<List<IMasterFontFamily>> GetMasterFontFamily(string masterFontFamilyName)
        {
            Log.Debug("Started GetMasterFontFamily Service");
            var result = await MasterFontRepository.GetMasterFontFamily(masterFontFamilyName);
            Log.Debug("Ended GetMasterFontFamily Service.");
            return result;
        }

        public async Task<bool> ImportGoogleFontFamily()
        {
            Log.Debug("Started ImportGoogleFontFamily Service");
            var response = GetGoogleFontFamily();
            if (response != null && response.FamilyMetadataList != null && response.FamilyMetadataList.Count > 0)
            {
                await MasterFontRepository.RemoveMasterFontFamilyBySource("Google");
                MasterFontFamily masterFontFamily = new MasterFontFamily();
                foreach (var item in response.FamilyMetadataList)
                {
                    masterFontFamily.Source = "Google";
                    masterFontFamily.Name = item.Family;
                    masterFontFamily.Category = item.Category;
                    masterFontFamily.SourceFontUrl = string.Format(Configuration.GoogleCssUrlTemplate, item.Family.Replace(" ", "+"));
                    masterFontFamily.FontWeight = new List<string>();
                    foreach (var font in item.Fonts)
                    {
                        masterFontFamily.FontWeight.Add(font.Key);
                    }
                    masterFontFamily.Designers = item.Designers;
                    //Add only if required fonts are available.
                    if (IsRequiredFontsAvailable(masterFontFamily.FontWeight))
                    {
                        await AddMasterFontFamily(masterFontFamily);
                    }
                }
            }
            Log.Debug("Ended ImportGoogleFontFamily Service");
            return true;
        }

        private IGoogleFontResponse GetGoogleFontFamily()
        {
            var request = new RestRequest(Configuration.GoogleFontApiEndPoint, Method.GET);
            return ExecuteRequest<GoogleFontResponse>(request);
        }

        private IRestResponse ExecuteRequest(IRestRequest request)
        {
            var baseUri = new Uri(request.Resource);
            RestClient client;
            request.Resource = string.Empty;
            client = new RestClient(baseUri);
            var response = client.Execute(request);
            if (response == null)
                throw new ArgumentNullException(nameof(response));
            return response;
        }

        private T ExecuteRequest<T>(IRestRequest request) where T : class
        {
            var response = ExecuteRequest(request);
            try
            {
                if (response.Content.Contains(")]}'"))
                {
                    response.Content = response.Content.Replace(")]}'", "");
                }
                return JsonConvert.DeserializeObject<T>(response.Content);
            }
            catch (Exception exception)
            {
                throw new Exception("Unable to deserialize:" + response.Content, exception);
            }
        }

        private bool IsRequiredFontsAvailable(List<string> availableFonts)
        {
            return !Configuration.RequiredFontWeight.Except(availableFonts).Any();
        }
    }
}