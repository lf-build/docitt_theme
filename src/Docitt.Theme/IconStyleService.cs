using LendFoundry.AssetManager;
using LendFoundry.Foundation.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Docitt.Theme
{
    public class IconStyleService : IIconStyleService
    {
        public IconStyleService(
            ILogger logger,
            IIconStyleRepository iconStyleRepository,
            IAssetManagerService assetManagerService,
            IConfiguration configuration)
        {
            Log = logger;
            IconStyleRepository = iconStyleRepository;
            Configuration = configuration;
            AssetManagerService = assetManagerService;
        }

        private ILogger Log { get; }
        private IIconStyleRepository IconStyleRepository { get; }
        private IAssetManagerService AssetManagerService { get; }
        private IConfiguration Configuration { get; }

        public async Task<IIconStyle> AddIconStyle(IIconStyle iconStyle)
        {
            Log.Debug("Started AddIconStyle Service.");
            var asset = await AssetManagerService.GetAll(Configuration.EntityTypeForIcons, iconStyle.Name);
            if (asset == null || asset.Count() == 0)
                throw new ArgumentException("No icon style files were found for EntityId " + iconStyle.Name);

            var result = await IconStyleRepository.AddIconStyle(iconStyle);
            if (result != null)
            {
                result.IconSourceUrl.AddRange(asset.Select(x => x.Url));
            }
            Log.Debug("Ended AddIconStyle Service.");
            return result;
        }

        public async Task<IIconStyle> GetDefaultIconStyle()
        {
            Log.Debug("Started GetDefaultIconStyle Service");
            var result = await IconStyleRepository.GetDefaultIconStyle();
            if (result != null)
            {
                var asset = await AssetManagerService.GetAll(Configuration.EntityTypeForIcons, result.Name);
                if (asset != null && asset.Count() > 0)
                {
                    result.IconSourceUrl.AddRange(asset.Select(x => x.Url));
                }
            }
            Log.Debug("Ended GetDefaultIconStyle Servoce.");
            return result;
        }
        public async Task<IIconStyle> GetCurrentIconStyle()
        {
            Log.Debug("Started GetCurrenttIconStyle Service");
            var result = await IconStyleRepository.GetCurrentIconStyle();
            if (result != null)
            {
                var asset = await AssetManagerService.GetAll(Configuration.EntityTypeForIcons, result.Name);
                if (asset != null && asset.Count() > 0)
                {
                    result.IconSourceUrl.AddRange(asset.Select(x => x.Url));
                }
            }
            Log.Debug("Ended GetCurrenttIconStyle Servoce.");
            return result;
        }

        public async Task<List<IIconStyle>> GetAllIconStyle()
        {
            Log.Debug("Started GetAllIconStyle Service");
            var result = await IconStyleRepository.GetAllIconStyle();

            foreach (var item in result)
            {
                var asset = await AssetManagerService.GetAll(Configuration.EntityTypeForIcons, item.Name);
                item.IconSourceUrl.AddRange(asset.Select(x => x.Url));
            }

            Log.Debug("Ended GetAllIconStyle Servoce.");
            return result;
        }

        public async Task<bool> ApplyIconStyle(string iconStyleName)
        {
            Log.Debug("Started ApplyIconStyle Service");
            var result = await IconStyleRepository.ApplyIconStyle(iconStyleName);
            Log.Debug("Ended ApplyIconStyle Servoce.");
            return result;
        }
    }
}