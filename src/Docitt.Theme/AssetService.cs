using LendFoundry.AssetManager;
using LendFoundry.Foundation.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Docitt.Theme
{
    public class AssetService : IAssetService
    {
        public AssetService(
            ILogger logger,
            IAssetRepository assetRepository,
            IAssetManagerService assetManagerService,
            IConfiguration configuration)
        {
            Log = logger;
            AssetRepository = assetRepository;
            Configuration = configuration;
            AssetManagerService = assetManagerService;
        }

        private ILogger Log { get; }
        private IAssetRepository AssetRepository { get; }
        private IAssetManagerService AssetManagerService { get; }
        private IConfiguration Configuration { get; }

        public async Task<IAsset> AddAsset(IAsset asset)
        {
            Log.Debug("Started AddAsset Service.");
            var assetResult = await AssetManagerService.GetAll(Configuration.EntityTypeForAssets, Configuration.EntityIdForAssets);
            if (assetResult != null && assetResult.Count() > 0)
            {
                if (!string.IsNullOrWhiteSpace(asset.WelcomScreenImageName) && assetResult.Where(x => x.FileName == asset.WelcomScreenImageName).FirstOrDefault() == null)
                {
                    throw new ArgumentNullException("No Welcome Screen Image found with the name " + asset.WelcomScreenImageName);
                }
                if (!string.IsNullOrWhiteSpace(asset.WelcomScreenImageName) && assetResult.Where(x => x.FileName == asset.WelcomeScreenBackgroundImageName).FirstOrDefault() == null)
                {
                    throw new ArgumentNullException("No welcome screen background image found with the name " + asset.WelcomeScreenBackgroundImageName);
                }
            }
            else
            {
                throw new ArgumentException("No assets were found for EntityId " + Configuration.EntityIdForAssets);
            }
            var result = await AssetRepository.AddAsset(asset);
            if (result != null)
            {
                if (!string.IsNullOrWhiteSpace(result.WelcomScreenImageName))
                {
                    result.WelcomScreenImageUrl = assetResult.Where(x => x.FileName == result.WelcomScreenImageName).FirstOrDefault() != null ? assetResult.Where(x => x.FileName == result.WelcomScreenImageName).FirstOrDefault().Url : string.Empty;
                }
                if (!string.IsNullOrWhiteSpace(result.WelcomeScreenBackgroundImageName))
                {
                    result.WelcomeScreenBackgroundImageUrl = assetResult.Where(x => x.FileName == result.WelcomeScreenBackgroundImageName).FirstOrDefault() != null ? assetResult.Where(x => x.FileName == result.WelcomeScreenBackgroundImageName).FirstOrDefault().Url : string.Empty;
                }
            }
            Log.Debug("Ended AddAsset Service.");
            return result;
        }

        public async Task<IAsset> GetDefaultAsset()
        {
            Log.Debug("Started GetDefaultAsset Service");
            var result = await AssetRepository.GetDefaultAsset();
            if (result != null)
            {
                var assetResult = await AssetManagerService.GetAll(Configuration.EntityTypeForAssets, Configuration.EntityIdForAssets);
                if (assetResult != null && assetResult.Count() > 0)
                {
                    if (!string.IsNullOrWhiteSpace(result.WelcomScreenImageName))
                    {
                        result.WelcomScreenImageUrl = assetResult.Where(x => x.FileName == result.WelcomScreenImageName).FirstOrDefault() != null ? assetResult.Where(x => x.FileName == result.WelcomScreenImageName).FirstOrDefault().Url : string.Empty;
                    }
                    if (!string.IsNullOrWhiteSpace(result.WelcomeScreenBackgroundImageName))
                    {
                        result.WelcomeScreenBackgroundImageUrl = assetResult.Where(x => x.FileName == result.WelcomeScreenBackgroundImageName).FirstOrDefault() != null ? assetResult.Where(x => x.FileName == result.WelcomeScreenBackgroundImageName).FirstOrDefault().Url : string.Empty;
                    }
                    if (!string.IsNullOrWhiteSpace(result.AlternateUserLogo))
                    {
                        result.AlternateUserLogoUrl = assetResult.Where(x => x.FileName == result.AlternateUserLogo).FirstOrDefault() != null ? assetResult.Where(x => x.FileName == result.AlternateUserLogo).FirstOrDefault().Url : string.Empty;
                    }
                }
            }
            Log.Debug("Ended GetDefaultAsset Service.");
            return result;
        }

        public async Task<IAsset> GetCurrentAsset()
        {
            Log.Debug("Started GetCurrentAsset Service");
            var result = await AssetRepository.GetCurrentAsset();
            if (result != null)
            {
                var assetResult = await AssetManagerService.GetAll(Configuration.EntityTypeForAssets, Configuration.EntityIdForAssets);
                if (assetResult != null && assetResult.Count() > 0)
                {
                    if (!string.IsNullOrWhiteSpace(result.WelcomScreenImageName))
                    {
                        result.WelcomScreenImageUrl = assetResult.Where(x => x.FileName == result.WelcomScreenImageName).FirstOrDefault() != null ? assetResult.Where(x => x.FileName == result.WelcomScreenImageName).FirstOrDefault().Url : string.Empty;
                    }
                    if (!string.IsNullOrWhiteSpace(result.WelcomeScreenBackgroundImageName))
                    {
                        result.WelcomeScreenBackgroundImageUrl = assetResult.Where(x => x.FileName == result.WelcomeScreenBackgroundImageName).FirstOrDefault() != null ? assetResult.Where(x => x.FileName == result.WelcomeScreenBackgroundImageName).FirstOrDefault().Url : string.Empty;
                    }
                }
            }
            Log.Debug("Ended GetCurrentAsset Service.");
            return result;
        }

        public async Task<List<IAsset>> GetAllAssets()
        {
            Log.Debug("Started GetAllAssets Service");
            var result = await AssetRepository.GetAllAssets();
            if (result != null)
            {
                var assetResult = await AssetManagerService.GetAll(Configuration.EntityTypeForAssets, Configuration.EntityIdForAssets);
                if (assetResult != null && assetResult.Count() > 0)
                {
                    foreach (var item in result)
                    {
                        if (!string.IsNullOrWhiteSpace(item.WelcomScreenImageName))
                        {
                            item.WelcomScreenImageUrl = assetResult.Where(x => x.FileName == item.WelcomScreenImageName).FirstOrDefault() != null ? assetResult.Where(x => x.FileName == item.WelcomScreenImageName).FirstOrDefault().Url : string.Empty;
                        }
                        if (!string.IsNullOrWhiteSpace(item.WelcomeScreenBackgroundImageName))
                        {
                            item.WelcomeScreenBackgroundImageUrl = assetResult.Where(x => x.FileName == item.WelcomeScreenBackgroundImageName).FirstOrDefault() != null ? assetResult.Where(x => x.FileName == item.WelcomeScreenBackgroundImageName).FirstOrDefault().Url : string.Empty;
                        }
                    }
                }
            }
            Log.Debug("Ended GetAllAssets Service.");
            return result;
        }
    }
}