using LendFoundry.AssetManager;
using LendFoundry.Foundation.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Docitt.Theme
{
    public class LogoService : ILogoService
    {
        public LogoService(
            ILogger logger,
            ILogoRepository logoRepository,
            IAssetManagerService assetManagerService,
            IConfiguration configuration)
        {
            Log = logger;
            LogoRepository = logoRepository;
            Configuration = configuration;
            AssetManagerService = assetManagerService;
        }

        private ILogger Log { get; }
        private ILogoRepository LogoRepository { get; }
        private IAssetManagerService AssetManagerService { get; }
        private IConfiguration Configuration { get; }

        public async Task<ILogo> AddLogo(ILogo logo)
        {
            Log.Debug("Started AddLogo Service.");
            var asset = await AssetManagerService.GetAll(Configuration.EntityTypeForAssets, Configuration.EntityIdForAssets);
            if (asset != null && asset.Count() > 0)
            {
                if (asset.Where(x => x.FileName == logo.LogoName).FirstOrDefault() == null)
                    throw new ArgumentNullException("No logo found with the name " + logo.LogoName);
                if (asset.Where(x => x.FileName == logo.BadgeLogoName).FirstOrDefault() == null)
                    throw new ArgumentNullException("No badgelogo found with the name " + logo.BadgeLogoName);
                if (asset.Where(x => x.FileName == logo.FavIconName).FirstOrDefault() == null)
                    throw new ArgumentNullException("No favicon found with the name " + logo.FavIconName);
            }
            else
            {
                throw new ArgumentException("No assets were found for EntityId " + Configuration.EntityIdForAssets);
            }

            var result = await LogoRepository.AddLogo(logo);
            if (result != null)
            {
                result.LogoUrl = asset.Where(x => x.FileName == result.LogoName).FirstOrDefault() != null ? asset.Where(x => x.FileName == result.LogoName).FirstOrDefault().Url : string.Empty;
                result.BadgeLogoUrl = asset.Where(x => x.FileName == result.BadgeLogoName).FirstOrDefault() != null ? asset.Where(x => x.FileName == result.BadgeLogoName).FirstOrDefault().Url : string.Empty;
                result.FavIconUrl = asset.Where(x => x.FileName == result.FavIconName).FirstOrDefault() != null ? asset.Where(x => x.FileName == result.FavIconName).FirstOrDefault().Url : string.Empty;
            }
            Log.Debug("Ended AddLogo Service.");
            return result;
        }

        public async Task<ILogo> GetDefaultLogo()
        {
            Log.Debug("Started GetDefaultLogo Service");
            var result = await LogoRepository.GetDefaultLogo();

            if (result != null)
            {
                var asset = await AssetManagerService.GetAll(Configuration.EntityTypeForAssets, Configuration.EntityIdForAssets);
                if (asset != null && asset.Count() > 0)
                {
                    result.LogoUrl = asset.Where(x => x.FileName == result.LogoName).FirstOrDefault() != null ? asset.Where(x => x.FileName == result.LogoName).FirstOrDefault().Url : string.Empty;
                    result.BadgeLogoUrl = asset.Where(x => x.FileName == result.BadgeLogoName).FirstOrDefault() != null ? asset.Where(x => x.FileName == result.BadgeLogoName).FirstOrDefault().Url : string.Empty;
                    result.FavIconUrl = asset.Where(x => x.FileName == result.FavIconName).FirstOrDefault() != null ? asset.Where(x => x.FileName == result.FavIconName).FirstOrDefault().Url : string.Empty;
                }
            }

            Log.Debug("Ended GetDefaultLogo Servoce.");
            return result;
        }

        public async Task<ILogo> GetCurrentLogo()
        {
            Log.Debug("Started GetCurrentLogo Service");
            var result = await LogoRepository.GetCurrentLogo();
            if (result != null)
            {
                var asset = await AssetManagerService.GetAll(Configuration.EntityTypeForAssets, Configuration.EntityIdForAssets);
                if (asset != null && asset.Count() > 0)
                {
                    result.LogoUrl = asset.Where(x => x.FileName == result.LogoName).FirstOrDefault() != null ? asset.Where(x => x.FileName == result.LogoName).FirstOrDefault().Url : string.Empty;
                    result.BadgeLogoUrl = asset.Where(x => x.FileName == result.BadgeLogoName).FirstOrDefault() != null ? asset.Where(x => x.FileName == result.BadgeLogoName).FirstOrDefault().Url : string.Empty;
                    result.FavIconUrl = asset.Where(x => x.FileName == result.FavIconName).FirstOrDefault() != null ? asset.Where(x => x.FileName == result.FavIconName).FirstOrDefault().Url : string.Empty;
                }
            }
            Log.Debug("Ended GetCurrentLogo Servoce.");
            return result;
        }

        public async Task<List<ILogo>> GetAllLogos()
        {
            Log.Debug("Started GetAllLogos Service");
            var result = await LogoRepository.GetAllLogos();
            if (result != null)
            {
                var asset = await AssetManagerService.GetAll(Configuration.EntityTypeForAssets, Configuration.EntityIdForAssets);
                if (asset != null && asset.Count() > 0)
                {
                    foreach (var item in result)
                    {
                        item.LogoUrl = asset.Where(x => x.FileName == item.LogoName).FirstOrDefault() != null ? asset.Where(x => x.FileName == item.LogoName).FirstOrDefault().Url : string.Empty;
                        item.BadgeLogoUrl = asset.Where(x => x.FileName == item.BadgeLogoName).FirstOrDefault() != null ? asset.Where(x => x.FileName == item.BadgeLogoName).FirstOrDefault().Url : string.Empty;
                        item.FavIconUrl = asset.Where(x => x.FileName == item.FavIconName).FirstOrDefault() != null ? asset.Where(x => x.FileName == item.FavIconName).FirstOrDefault().Url : string.Empty;
                    }
                }
            }
            Log.Debug("Ended GetAllLogos Servoce.");
            return result;
        }
    }
}