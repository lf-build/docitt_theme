using LendFoundry.Foundation.Logging;
using System.Threading.Tasks;

namespace Docitt.Theme
{
    public class TenantFontFamilyService : ITenantFontFamilyService
    {
        public TenantFontFamilyService(
            ILogger logger,
            IConfiguration configuration,
            ITenantFontFamilyRepository tenantFontRepository)
        {
            Log = logger;
            TenantFontRepository = tenantFontRepository;
            Configuration = configuration;
        }

        private ILogger Log { get; }

        private ITenantFontFamilyRepository TenantFontRepository { get; }
        private IConfiguration Configuration { get; }

        public async Task<ITenantFontFamily> ApplyTenantFontFamily(ITenantFontFamily masterFontFamily)
        {
            Log.Debug("Started ApplyTenantFontFamily Service.");
            var result = await TenantFontRepository.ApplyTenantFontFamily(masterFontFamily);
            Log.Debug("Ended ApplyTenantFontFamily Service.");
            return result;
        }

        public async Task<ITenantFontFamily> GetTenantFontFamily()
        {
            Log.Debug("Started GetTenantFontFamily Service");
            var result = await TenantFontRepository.GetTenantFontFamily();
            Log.Debug("Ended GetTenantFontFamily Service.");
            return result;
        }
    }
}