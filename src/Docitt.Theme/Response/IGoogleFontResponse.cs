﻿using LendFoundry.Foundation.Client;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace Docitt.Theme
{
    public interface IGoogleFontResponse
    {
        IList<IGoogleFontFamily> FamilyMetadataList { get; set; }
    }

    public class GoogleFontResponse : IGoogleFontResponse
    {
        [JsonProperty("familyMetadataList")]
        [JsonConverter(typeof(InterfaceListConverter<IGoogleFontFamily, GoogleFontFamily>))]
        public IList<IGoogleFontFamily> FamilyMetadataList { get; set; }
    }

    public interface IGoogleFontFamily
    {
        string Family { get; set; }
        string Category { get; set; }
        List<string> Designers { get; set; }
        Dictionary<string, Font> Fonts { get; set; }
    }

    public class GoogleFontFamily : IGoogleFontFamily
    {
        [JsonProperty("family")]
        public string Family { get; set; }
        [JsonProperty("category")]
        public string Category { get; set; }
        [JsonProperty("designers")]
        public List<string> Designers { get; set; }
        [JsonProperty("fonts")]
        public Dictionary<string, Font> Fonts { get; set; }
    }

    public interface IFont
    {
        int Thickness { get; set; }
        int Slant { get; set; }
        int Width { get; set; }
    }

    public class Font : IFont
    {
        [JsonProperty("thickness")]
        public int Thickness { get; set; }

        [JsonProperty("slant")]
        public int Slant { get; set; }

        [JsonProperty("width")]
        public int Width { get; set; }
    }

}
