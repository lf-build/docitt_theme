using System;
namespace Docitt.Theme
{
    public static class Settings
    {
        public static string ServiceName => Environment.GetEnvironmentVariable($"CONFIGURATION_NAME") ?? "theme";
        public static string TemporaryFolder { get; } = Environment.GetEnvironmentVariable("TEMPFOLDER") ?? "/tmp";
        public static string BasePath { get; } = Environment.GetEnvironmentVariable("BASEPATH") ?? "/usr/bin/";
    }
}