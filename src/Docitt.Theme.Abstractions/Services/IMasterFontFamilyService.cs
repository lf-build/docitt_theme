using System.Collections.Generic;
using System.Threading.Tasks;

namespace Docitt.Theme
{
    public interface IMasterFontFamilyService
    {
        Task<IMasterFontFamily> GetDefaultMasterFontFamily();
        Task<IMasterFontFamily> SetDefaultMasterFontFamily(string masterFontFamilyName);
        Task<List<IMasterFontFamily>> GetMasterFontFamily(string masterFontFamilyName = "");
        Task<bool> AddMasterFontFamily(IMasterFontFamily masterFontFamily);
        Task<bool> ImportGoogleFontFamily();
    }
}