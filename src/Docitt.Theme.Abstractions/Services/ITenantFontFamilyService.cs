﻿using System.Threading.Tasks;

namespace Docitt.Theme
{
    public interface ITenantFontFamilyService
    {
        Task<ITenantFontFamily> ApplyTenantFontFamily(ITenantFontFamily tenantFontFamily);
        Task<ITenantFontFamily> GetTenantFontFamily();
    }
}
