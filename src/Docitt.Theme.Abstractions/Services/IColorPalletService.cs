using System.Collections.Generic;
using System.Threading.Tasks;

namespace Docitt.Theme
{
    public interface IColorPalletService
    {
        Task<IColorPallet> AddColorPallet(IColorPallet colorPallet);
        Task<IColorPallet> GetDefaultColorPallet();
        Task<List<IColorPallet>> GetSystemDefinedColorPallet();
        Task<IColorPallet> GetCurrentColorPallet();
        Task<List<IColorPallet>> GetAllColorPallet();
        Task<bool> ApplyColorPallet(string colorPalletName);
    }
}