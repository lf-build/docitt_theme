﻿using System.Threading.Tasks;

namespace Docitt.Theme
{
    public interface ICSSGenerator
    {
        Task<bool> GenerateCSS();
    }
}
