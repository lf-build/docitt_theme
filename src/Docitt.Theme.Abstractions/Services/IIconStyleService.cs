using System.Collections.Generic;
using System.Threading.Tasks;

namespace Docitt.Theme
{
    public interface IIconStyleService
    {
        Task<IIconStyle> GetDefaultIconStyle();
        Task<IIconStyle> GetCurrentIconStyle();
        Task<List<IIconStyle>> GetAllIconStyle();
        Task<IIconStyle> AddIconStyle(IIconStyle iconStyle);
        Task<bool> ApplyIconStyle(string iconStyleName);
    }
}