using LendFoundry.Foundation.Persistence;
using System;
using System.Collections.Generic;

namespace Docitt.Theme
{
    public interface IMasterFontFamily : IAggregate
    {
        string Name { get; set; }
        string Category { get; set; }
        string Source { get; set; }
        string SourceFontUrl { get; set; }
        List<string> FontWeight { get; set; }
        List<string> Designers { get; set; }
        bool IsDefault { get; set; }
        string CreatedBy { get; set; }
        DateTimeOffset CreatedOn { get; set; }
        string UpdatedBy { get; set; }
        DateTimeOffset? UpdatedOn { get; set; }
    }
}