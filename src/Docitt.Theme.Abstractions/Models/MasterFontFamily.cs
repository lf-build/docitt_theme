﻿using LendFoundry.Foundation.Persistence;
using System;
using System.Collections.Generic;

namespace Docitt.Theme
{
    public class MasterFontFamily : Aggregate, IMasterFontFamily
    {
        public MasterFontFamily()
        {

        }

        public string Name { get; set; }
        public string Category { get; set; }
        public string Source { get; set; }
        public string SourceFontUrl { get; set; }
        public List<string> FontWeight { get; set; }
        public List<string> Designers { get; set; }
        public bool IsDefault { get; set; }
        public string CreatedBy { get; set; }
        public DateTimeOffset CreatedOn { get; set; }
        public string UpdatedBy { get; set; }
        public DateTimeOffset? UpdatedOn { get; set; }
    }
}
