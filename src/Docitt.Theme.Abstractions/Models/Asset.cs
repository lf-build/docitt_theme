﻿using LendFoundry.Foundation.Persistence;
using System;

namespace Docitt.Theme
{
    public class Asset : Aggregate, IAsset
    {
        public Asset()
        {

        }
        public string WelcomeScreenBackgroundImageUrl { get; set; }
        public string WelcomeScreenBackgroundImageName { get; set; }
        public bool IsWelcomeScreenBackgroundImageOn { get; set; }
        public string WelcomScreenVideoUrl { get; set; }
        public bool IsWelcomScreenVideoOn { get; set; }
        public string WelcomScreenImageUrl { get; set; }
        public string WelcomScreenImageName { get; set; }
        public bool IsDefault { get; set; }
        public bool IsApplied { get; set; }
        public string AppliedBy { get; set; }
        public DateTimeOffset? AppliedOn { get; set; }
        public string AlternateUserLogo { get; set; }
        public string AlternateUserLogoUrl { get; set; }
    }
}
