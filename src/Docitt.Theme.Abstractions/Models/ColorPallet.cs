using LendFoundry.Foundation.Persistence;
using LendFoundry.Foundation.Client;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace Docitt.Theme
{
    public class ColorPallet : Aggregate,  IColorPallet
    {
        public ColorPallet()
        {

        }

        public string Name { get; set; }
        [JsonConverter(typeof(InterfaceListConverter<IColor, Color>))]
        public List<IColor> Colors { get; set; }
        public bool IsDefault { get; set; }
        public bool IsSystemDefined { get; set; }
        public bool IsApplied { get; set; }
        public string CreatedBy { get; set; }
        public DateTimeOffset CreatedOn { get; set; }
        public string UpdatedBy { get; set; }
        public DateTimeOffset? UpdatedOn { get; set; }
        public string AppliedBy { get; set; }
        public DateTimeOffset? AppliedOn { get; set; }
    }
}