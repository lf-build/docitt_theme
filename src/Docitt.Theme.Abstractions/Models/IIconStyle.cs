using LendFoundry.Foundation.Persistence;
using System;
using System.Collections.Generic;

namespace Docitt.Theme
{
    public interface IIconStyle : IAggregate
    {
        string Name { get; set; }
        List<string> IconSourceUrl { get; set; }
        bool IsDefault { get; set; }
        bool IsApplied { get; set; }
        string CreatedBy { get; set; }
        DateTimeOffset? CreatedOn { get; set; }
        string UpdatedBy { get; set; }
        DateTimeOffset? UpdatedOn { get; set; }
        string AppliedBy { get; set; }
        DateTimeOffset? AppliedOn { get; set; }
    }
}