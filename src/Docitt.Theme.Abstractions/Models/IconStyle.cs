using LendFoundry.Foundation.Persistence;
using System;
using System.Collections.Generic;

namespace Docitt.Theme
{
    public class IconStyle : Aggregate, IIconStyle
    {
        public IconStyle()
        {

        }

        public string Name { get; set; }
        public List<string> IconSourceUrl { get; set; } = new List<string>();
        public bool IsDefault { get; set; }
        public bool IsApplied { get; set; }
        public string CreatedBy { get; set; }
        public DateTimeOffset? CreatedOn { get; set; }
        public string UpdatedBy { get; set; }
        public DateTimeOffset? UpdatedOn { get; set; }
        public string AppliedBy { get; set; }
        public DateTimeOffset? AppliedOn { get; set; }
    }
}