namespace Docitt.Theme
{
    public interface IColor
    {
        string Name { get; set; }
        string Code { get; set; }
    }
}