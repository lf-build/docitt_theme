﻿using LendFoundry.Foundation.Persistence;
using System;
using System.Collections.Generic;

namespace Docitt.Theme
{
    public class TenantFontFamily : Aggregate, ITenantFontFamily
    {
        public TenantFontFamily()
        {

        }
        public string Name { get; set; }
        public string SourceFontUrl { get; set; }
        public List<string> FontWeight { get; set; }
        public string AppliedBy { get; set; }
        public DateTimeOffset AppliedOn { get; set; }
    }
}
