﻿using LendFoundry.Foundation.Client;
using System.Collections.Generic;

namespace Docitt.Theme
{
    public interface IConfiguration : IDependencyConfiguration
    {
        string GoogleFontApiEndPoint { get; set; }
        string GoogleCssUrlTemplate { get; set; }
        string EntityTypeForAssets { get; set; }
        string EntityIdForAssets { get; set; }
        string EntityTypeForIcons { get; set; }
        string CssCompilerCommand { get; set; }
        string CssCompilerArgs { get; set; }
        string CssMinifierCommand { get; set; }
        string CssMinifierArgs { get; set; }
        List<string> RequiredFontWeight { get; set; }
        string AssetBaseURL { get; set; }
    }
}
