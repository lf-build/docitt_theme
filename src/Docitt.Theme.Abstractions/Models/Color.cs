namespace Docitt.Theme
{
    public class Color : IColor
    {
        public string Name { get; set; }
        public string Code { get; set; }
    }
}