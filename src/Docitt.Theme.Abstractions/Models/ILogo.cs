﻿using LendFoundry.Foundation.Persistence;
using System;

namespace Docitt.Theme
{
    public interface ILogo : IAggregate
    {
        string LogoName { get; set; }
        string LogoUrl { get; set; }
        string BadgeLogoName { get; set; }
        string BadgeLogoUrl { get; set; }
        string FavIconName { get; set; }
        string FavIconUrl { get; set; }
        bool IsDefault { get; set; }
        bool IsApplied { get; set; }
        string AppliedBy { get; set; }
        DateTimeOffset? AppliedOn { get; set; }
    }
}
