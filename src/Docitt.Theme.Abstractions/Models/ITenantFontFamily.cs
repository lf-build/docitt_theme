using LendFoundry.Foundation.Persistence;
using System;
using System.Collections.Generic;

namespace Docitt.Theme
{
    public interface ITenantFontFamily : IAggregate
    {
        string Name { get; set; }
        string SourceFontUrl { get; set; }
        List<string> FontWeight { get; set; }
        string AppliedBy { get; set; }
        DateTimeOffset AppliedOn { get; set; }
    }
}