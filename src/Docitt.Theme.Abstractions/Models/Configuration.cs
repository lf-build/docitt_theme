﻿using System.Collections.Generic;

namespace Docitt.Theme
{
    public class Configuration : IConfiguration
    {
        public Dictionary<string, string> Dependencies { get; set; }
        public string Database { get; set; }
        public string GoogleFontApiEndPoint { get; set; }
        public string GoogleCssUrlTemplate { get; set; }
        public string EntityTypeForAssets { get; set; }
        public string EntityIdForAssets { get; set; }
        public string EntityTypeForIcons { get; set; }
        public string CssCompilerCommand { get; set; }
        public string CssCompilerArgs { get; set; }
        public string CssMinifierCommand { get; set; }
        public string CssMinifierArgs { get; set; }
        public List<string> RequiredFontWeight { get; set; }
        public string AssetBaseURL { get; set; }
    }
}
