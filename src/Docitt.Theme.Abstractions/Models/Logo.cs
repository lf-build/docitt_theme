﻿using LendFoundry.Foundation.Persistence;
using System;

namespace Docitt.Theme
{
    public class Logo : Aggregate, ILogo
    {
        public Logo()
        {

        }
        public string LogoName { get; set; }
        public string LogoUrl { get; set; }
        public string BadgeLogoName { get; set; }
        public string BadgeLogoUrl { get; set; }
        public string FavIconName { get; set; }
        public string FavIconUrl { get; set; }
        public bool IsDefault { get; set; }
        public bool IsApplied { get; set; }
        public string AppliedBy { get; set; }
        public DateTimeOffset? AppliedOn { get; set; }
    }
}
