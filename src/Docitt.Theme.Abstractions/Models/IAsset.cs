﻿using LendFoundry.Foundation.Persistence;
using System;

namespace Docitt.Theme
{
    public interface IAsset : IAggregate
    {
        string WelcomeScreenBackgroundImageUrl { get; set; }
        string WelcomeScreenBackgroundImageName { get; set; }
        bool IsWelcomeScreenBackgroundImageOn { get; set; }
        string WelcomScreenVideoUrl { get; set; }
        bool IsWelcomScreenVideoOn { get; set; }
        string WelcomScreenImageUrl { get; set; }
        string WelcomScreenImageName { get; set; }
        bool IsDefault { get; set; }
        bool IsApplied { get; set; }
        string AppliedBy { get; set; }
        DateTimeOffset? AppliedOn { get; set; }
        string AlternateUserLogo { get; set; }
        string AlternateUserLogoUrl { get; set; }
    }
}
