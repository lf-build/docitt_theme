﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace Docitt.Theme
{
    public interface IAssetRepository
    {
        Task<IAsset> GetDefaultAsset();
        Task<IAsset> GetCurrentAsset();
        Task<IAsset> AddAsset(IAsset asset);
        Task<List<IAsset>> GetAllAssets();
    }
}
