using LendFoundry.Foundation.Persistence;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Docitt.Theme
{
    public interface IIconStyleRepository : IRepository<IIconStyle>
    {
        Task<IIconStyle> GetDefaultIconStyle();
        Task<List<IIconStyle>> GetIconStyleByName(string iconStyleName);
        Task<List<IIconStyle>> GetAllIconStyle();
        Task<IIconStyle> AddIconStyle(IIconStyle iconStyle);
        Task<bool> ApplyIconStyle(string iconStyleName);
        Task<IIconStyle> GetCurrentIconStyle();
    }
}