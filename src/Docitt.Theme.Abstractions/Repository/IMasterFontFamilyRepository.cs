using LendFoundry.Foundation.Persistence;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Docitt.Theme
{
    public interface IMasterFontFamilyRepository : IRepository<IMasterFontFamily>
    {
        Task<IMasterFontFamily> SetDefaultMasterFontFamily(string masterFontFamilyName);
        Task<IMasterFontFamily> GetDefaultMasterFontFamily();
        Task<List<IMasterFontFamily>> GetMasterFontFamily(string masterFontFamilyName);
        Task<bool> AddMasterFontFamily(IMasterFontFamily masterFontFamily);
        Task RemoveMasterFontFamilyBySource(string sourceName);
    }
}