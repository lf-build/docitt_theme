using LendFoundry.Foundation.Persistence;
using System.Threading.Tasks;

namespace Docitt.Theme
{
    public interface ITenantFontFamilyRepository : IRepository<ITenantFontFamily>
    {
        Task<ITenantFontFamily> ApplyTenantFontFamily(ITenantFontFamily tenantFontFamily);
        Task<ITenantFontFamily> GetTenantFontFamily();
    }
}