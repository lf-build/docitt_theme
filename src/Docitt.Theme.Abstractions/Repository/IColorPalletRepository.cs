using LendFoundry.Foundation.Persistence;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Docitt.Theme
{
    public interface IColorPalletRepository : IRepository<IColorPallet>
    {
        Task<IColorPallet> GetDefaultColorPallet();
        Task<List<IColorPallet>> GetSystemDefinedColorPallet();
        Task<IColorPallet> GetCurrentColorPallet();
        Task<List<IColorPallet>> GetColorPalletByName(string colorPalletName);
        Task<List<IColorPallet>> GetAllColorPallet();
        Task<IColorPallet> AddColorPallet(IColorPallet colorPallet);
        Task<bool> ApplyColorPallet(string colorPalletName);
    }
}