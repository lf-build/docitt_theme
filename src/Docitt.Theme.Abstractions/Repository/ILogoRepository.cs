﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace Docitt.Theme
{
    public interface ILogoRepository
    {
        Task<ILogo> GetDefaultLogo();
        Task<ILogo> GetCurrentLogo();
        Task<ILogo> AddLogo(ILogo logo);
        Task<List<ILogo>> GetAllLogos();
    }
}
